from PyQt5 import QtWidgets
from Hybrid import Hybrid
from generated_ui.fec_frame_lci import Ui_Form as FecForm


class FEC(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.ui = FecForm()
        self.ui.setupUi(self)
        self.connect_fec_window_ports()
        self.ui.hybrid_tabWidget.tabBarClicked.connect(self.update_channel_settings)
        self.ui.hybrid_tabWidget.currentChanged.connect(self.reload_channel_settings)

    def update_channel_settings(self):
        self.window().update_channel_settings()

    def reload_channel_settings(self):
        hybrid = self.ui.hybrid_tabWidget.currentWidget()
        if hybrid is None:
            main_window = self.window()
            main_window.channels_widget.hide()
            return
        hybrid.reload_channel_settings()

    def connect_fec_window_ports(self):
        self.ui.hybrid0_checkBox.stateChanged.connect(lambda: self.hybrid_state_changed(0))
        self.ui.hybrid1_checkBox.stateChanged.connect(lambda: self.hybrid_state_changed(1))
        self.ui.hybrid2_checkBox.stateChanged.connect(lambda: self.hybrid_state_changed(2))
        self.ui.hybrid3_checkBox.stateChanged.connect(lambda: self.hybrid_state_changed(3))
        self.ui.hybrid4_checkBox.stateChanged.connect(lambda: self.hybrid_state_changed(4))
        self.ui.acq_pushButton.clicked.connect(self.acq_state_changed)

    def acq_state_changed(self):
        if self.ui.acq_pushButton.isChecked():
            self.window().ui.send_pushButton.setEnabled(False)
            # TODO: begin acquisition
        else:
            self.window().ui.send_pushButton.setEnabled(True)
            # TODO: stop acquisition

    def create_hybrid_tab(self, tab_idx):
        count = self.ui.hybrid_tabWidget.count()
        # print("create_hybrid_tab, hybrids count: " + str(count))
        # create tabs if necessary, if there is no tabs and we need Hybrid 1, then Hybrid 0 will be added too
        for tab in range(count, tab_idx + 1):
            self.ui.hybrid_tabWidget.insertTab(tab, Hybrid(self.ui.hybrid_tabWidget), "Hybrid_" + str(tab))
            self.ui.hybrid_tabWidget.setTabVisible(tab, False)

    def hybrid_state_changed(self, hybrid_idx):
        is_checked = False
        if hybrid_idx == 0:
            is_checked = self.ui.hybrid0_checkBox.isChecked()
        elif hybrid_idx == 1:
            is_checked = self.ui.hybrid1_checkBox.isChecked()
        elif hybrid_idx == 2:
            is_checked = self.ui.hybrid2_checkBox.isChecked()
        elif hybrid_idx == 3:
            is_checked = self.ui.hybrid3_checkBox.isChecked()
        elif hybrid_idx == 4:
            is_checked = self.ui.hybrid4_checkBox.isChecked()

        if is_checked:
            self.create_hybrid_tab(hybrid_idx)
            self.ui.hybrid_tabWidget.setTabVisible(hybrid_idx, True)
            self.ui.hybrid_tabWidget.setCurrentWidget(self.ui.hybrid_tabWidget.widget(hybrid_idx))
        else:
            self.ui.hybrid_tabWidget.setTabVisible(hybrid_idx, False)

    def apply_default_settings(self):
        self.ui.hybrid0_checkBox.setChecked(True)
        self.ui.hybrid1_checkBox.setChecked(True)
        self.ui.hybrid2_checkBox.setChecked(True)
        self.ui.hybrid3_checkBox.setChecked(True)
        self.ui.hybrid4_checkBox.setChecked(True)

        count = self.ui.hybrid_tabWidget.count()
        for tab in range(0, count):  # for all active Hybrids
            self.ui.hybrid_tabWidget.widget(tab).apply_default_settings()


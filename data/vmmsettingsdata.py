from dataclasses import dataclass, field
from typing import List


def make_default_channel(idx: int = 0):
    return Channel(idx)


def make_default_channel_settings():
    return ChannelSettings()


def make_RegisterSPI0VMMBank1():
    return RegisterSPI0VMMBank1()


def make_RegisterSPI0VMMBank2():
    return RegisterSPI0VMMBank2()


def make_RegisterSPI1VMMBank1():
    return RegisterSPI1VMMBank1()


def make_RegisterSPI1VMMBank2():
    return RegisterSPI1VMMBank2()


def make_RegisterSPI2VMMBank1():
    return RegisterSPI2VMMBank1()


def make_RegisterSPI2VMMBank2():
    return RegisterSPI2VMMBank2()


def make_default_channel_list():
    return [Channel(idx) for idx in range(0, 64)]


def make_default_vmms_list():
    return VmmList()


def make_default_hybrids_list():
    return HybridList()


def make_default_hybrid_list():
    return [HybridData(idx) for idx in range(0, 0)]


def make_default_vmm_list():
    return [VmmData(idx) for idx in range(0, 0)]


def make_default_fec_list():
    return [FecData(idx) for idx in range(0, 0)]


@dataclass
class Channel:
    idx: int
    sc: int = 0
    sl: int = 0
    st: int = 1
    sth: int = 0
    sm: int = 0
    sd: int = 0
    smx: int = 0
    adc0_10: int = 0
    adc0_8: int = 0
    adc0_6: int = 0

    def sc_field(self): return (self.sc & 0x1) << 23
    def sl_field(self): return (self.sl & 0x1) << 22
    def st_field(self): return (self.st & 0x1) << 21
    def sth_field(self): return (self.sth & 0x1) << 20
    def sm_field(self): return (self.sm & 0x1) << 19
    def smx_field(self): return (self.smx & 0x1) << 18
    def sd_field(self): return (self.sd & 0x1F) << 13
    def adc0_10_field(self): return (self.adc0_10 & 0x1F) << 8
    def adc0_8_field(self): return (self.adc0_8 & 0xF) << 4
    def adc0_6_field(self): return (self.adc0_6 & 0x7) << 1

    def __index__(self):
        reg_val = 0
        reg_val |= \
            self.sc_field() | \
            self.sl_field() | \
            self.st_field() | \
            self.sth_field() | \
            self.sm_field() | \
            self.smx_field() | \
            self.sd_field() | \
            self.adc0_10_field() | \
            self.adc0_8_field() | \
            self.adc0_6_field()
        return reg_val


@dataclass
class ChannelSettings:
    channels: List[Channel] = field(default_factory=make_default_channel_list)

    def insert(self, idx, channel):
        if self.channels[idx] != channel:
            self.channels[idx] = channel

    def get_channel(self, idx):
        return self.channels[idx]

    def get_as_list(self):
        return self.channels

    def update_channel(self, channel: Channel):
        c = self.get_channel(channel.idx)
        c.sc = channel.sc
        c.sl = channel.sl
        c.st = channel.st
        c.sth = channel.sth
        c.sm = channel.sm
        c.sd = channel.sd
        c.smx = channel.smx
        c.adc0_10 = channel.adc0_10
        c.adc0_8 = channel.adc0_8
        c.adc0_6 = channel.adc0_6


@dataclass
class RegisterSPI0VMMBank1:
    address_hex: int = 43
    slvs: int = 0
    s32: int = 0
    stcr: int = 0
    ssart: int = 0
    srec: int = 0
    stlc: int = 0
    sbip: int = 0
    srat: int = 0
    sfrst: int = 0
    slvsbc: int = 0
    slvstp: int = 0
    slvstk: int = 0
    slvsdt: int = 0
    slvsart: int = 0
    slvstki: int = 0
    slvsena: int = 0
    slvs6b: int = 0
    sL0enaV: int = 0
    slh: int = 0
    slxh: int = 0
    stgc: int = 0
    reset1: int = 0
    reset2: int = 0

    def slvs_field(self): return (self.slvs & 0x1) << 27
    def s32_field(self): return (self.s32 & 0x1) << 26
    def stcr_field(self): return (self.stcr & 0x1) << 25
    def ssart_field(self): return (self.ssart & 0x1) << 24
    def srec_field(self): return (self.srec & 0x1) << 23
    def stlc_field(self): return (self.stlc & 0x1) << 22
    def sbip_field(self): return (self.sbip & 0x1) << 21
    def srat_field(self): return (self.srat  & 0x1) << 20
    def sfrst_field(self): return (self.sfrst & 0x1) << 19
    def slvsbc_field(self): return (self.slvsbc & 0x1) << 18
    def slvstp_field(self): return (self.slvstp & 0x1) << 17
    def slvstk_field(self): return (self.slvstk & 0x1) << 16
    def slvsdt_field(self): return (self.slvsdt & 0x1) << 15
    def slvsart_field(self): return (self.slvsart & 0x1) << 14
    def slvstki_field(self): return (self.slvstki & 0x1) << 13
    def slvsena_field(self): return (self.slvsena & 0x1) << 12
    def slvs6b_field(self): return (self.slvs6b & 0x1) << 11
    def sL0enaV_field(self): return (self.sL0enaV & 0x1) << 10
    def slh_field(self): return (self.slh & 0x1) << 9
    def slxh_field(self): return (self.slxh & 0x1) << 8
    def stgc_field(self): return (self.stgc & 0x1) << 7
    def reset1_field(self): return (self.reset1 & 0x1) << 1
    def reset2_field(self): return (self.reset2 & 0x1) << 0

    def __index__(self):
        reg_val = 0
        reg_val |= \
            self.slvs_field() | \
            self.s32_field() | \
            self.stcr_field() | \
            self.ssart_field() | \
            self.srec_field() | \
            self.stlc_field() | \
            self.sbip_field() | \
            self.srat_field() | \
            self.sfrst_field() | \
            self.slvsbc_field() | \
            self.slvstp_field() | \
            self.slvstk_field() | \
            self.slvsdt_field() | \
            self.slvsart_field() | \
            self.slvstki_field() | \
            self.slvsena_field() | \
            self.slvs6b_field() | \
            self.sL0enaV_field() | \
            self.slh_field() | \
            self.slxh_field() | \
            self.stgc_field() | \
            self.reset1_field() | \
            self.reset2_field()

        return reg_val


@dataclass
class RegisterSPI0VMMBank2:
    address_hex: int = 0
    nskipm_i: int = 0

    def nskipm_i_field(self): return (self.nskipm_i & 0x1) << 0

    def __index__(self):
        reg_val = 0
        reg_val |= \
            self.nskipm_i_field()
        return reg_val

@dataclass
class RegisterSPI1VMMBank1:
    address_hex: int = 44
    sdt_lower_6bits: int = 10
    sdp10: int = 300  # sdp_2
    sc10b: int = 0  # convtime_10
    sc8b: int = 0  # convtime_8
    sc6b: int = 0  # convtime_6
    s8b: int = 1
    s6b: int = 0
    s10b: int = 1
    sdcks: int = 0
    sdcka: int = 0
    sdck6b: int = 0
    sdrv: int = 0
    stpp: int = 0

    def sdt_lower_6bits_field(self): return (self.sdt_lower_6bits & 0x3F) << 26
    def sdp10_field(self): return (self.sdp10 & 0x3FF) << 16
    def sc10b_field(self): return (self.sc10b & 0x3) << 14
    def sc8b_field(self): return (self.sc8b & 0x3) << 12
    def sc6b_field(self): return (self.sc6b & 0x7) << 9
    def s8b_field(self): return (self.s8b & 0x1) << 8
    def s6b_field(self): return (self.s6b & 0x1) << 7
    def s10b_field(self): return (self.s10b & 0x1) << 6
    def sdcks_field(self): return (self.sdcks & 0x1) << 5
    def sdcka_field(self): return (self.sdcka & 0x1) << 4
    def sdck6b_field(self): return (self.sdck6b & 0x1) << 3
    def sdrv_field(self): return (self.sdrv & 0x1) << 2
    def stpp_field(self): return (self.stpp & 0x1) << 1

    def __index__(self):
        reg_val = 0
        reg_val |= \
            self.sdt_lower_6bits_field() | \
            self.sdp10_field() | \
            self.sc10b_field() | \
            self.sc8b_field() | \
            self.sc6b_field() | \
            self.s8b_field() | \
            self.s6b_field() | \
            self.s10b_field() | \
            self.sdcks_field() | \
            self.sdcka_field() | \
            self.sdck6b_field() | \
            self.sdrv_field() | \
            self.stpp_field()

        return reg_val

@dataclass
class RegisterSPI1VMMBank2:
    address_hex: int = 1
    sL0cktest: int = 0
    sL0dckinv: int = 0
    sL0ckinv: int = 0
    sL0ena: int = 0
    truncate_i: int = 0
    nskip_i: int = 0
    window_i: int = 0
    rollover_i: int = 0

    def sL0cktest_field(self): return (self.sL0cktest & 0x1) << 31
    def sL0dckinv_field(self): return (self.sL0dckinv & 0x1) << 30
    def sL0ckinv_field(self): return (self.sL0ckinv & 0x1) << 29
    def sL0ena_field(self): return (self.sL0ena & 0x1) << 28
    def truncate_i_field(self): return (self.truncate_i & 0x3F) << 22
    def nskip_i_field(self): return (self.nskip_i & 0x3F) << 15
    def window_i_field(self): return (self.window_i & 0x7) << 12
    def rollover_i_field(self): return (self.rollover_i & 0xFFF) << 0

    def __index__(self):
        reg_val = 0
        reg_val |= \
            self.sL0cktest_field() | \
            self.sL0dckinv_field() | \
            self.sL0ckinv_field() | \
            self.sL0ena_field() | \
            self.truncate_i_field() | \
            self.nskip_i_field() | \
            self.window_i_field() | \
            self.rollover_i_field()

        return reg_val

@dataclass
class RegisterSPI2VMMBank1:
    address_hex: int = 45
    sp: int = 0
    sdp: int = 0
    sbmx: int = 0
    sbft: int = 0
    sbfp: int = 0
    sbfm: int = 0
    slg: int = 0
    sm: int = 0  # monitoring
    scmx: int = 0
    sfa: int = 0
    sfam: int = 0
    st: int = 3  # peaktime
    sfm: int = 0
    sg: int = 7  # gain
    sng: int = 0
    stot: int = 0
    sttt: int = 0
    ssh: int = 0
    stc: int = 1
    sdt_upper_4bits: int = 5

    def sp_field(self): return (self.sp & 0x1) << 31
    def sdp_field(self): return (self.sdp & 0x1) << 30
    def sbmx_field(self): return (self.sbmx & 0x1) << 29
    def sbft_field(self): return (self.sbft & 0x1) << 28
    def sbfp_field(self): return (self.sbfp & 0x1) << 27
    def sbfm_field(self): return (self.sbfm & 0x1) << 26
    def slg_field(self): return (self.slg & 0x1) << 25
    def sm_field(self): return (self.sm & 0x3F) << 19
    def scmx_field(self): return (self.scmx & 0x1) << 18
    def sfa_field(self): return (self.sfa & 0x1) << 17
    def sfam_field(self): return (self.sfam & 0x1) << 16
    def st_field(self): return (self.st & 0x3) << 14
    def sfm_field(self): return (self.sfm & 0x1) << 13
    def sg_field(self): return (self.sg & 0x7) << 10
    def sng_field(self): return (self.sng & 0x1) << 9
    def stot_field(self): return (self.stot & 0x1) << 8
    def sttt_field(self): return (self.sttt & 0x1) << 7
    def ssh_field(self): return (self.ssh & 0x1) << 6
    def stc_field(self): return (self.stc & 0x1) << 4
    def sdt_upper_4bits_field(self): return (self.sdt_upper_4bits & 0x1) << 0

    def __index__(self):
        reg_val = 0
        reg_val |= \
            self.sp_field() | \
            self.sdp_field() | \
            self.sbmx_field() | \
            self.sbft_field() | \
            self.sbfp_field() | \
            self.sbfm_field() | \
            self.slg_field() | \
            self.sm_field() | \
            self.scmx_field() | \
            self.sfa_field() | \
            self.sfam_field() | \
            self.st_field() | \
            self.sfm_field() | \
            self.sg_field() | \
            self.sng_field() | \
            self.stot_field() | \
            self.sttt_field() | \
            self.ssh_field() | \
            self.stc_field() | \
            self.sdt_upper_4bits_field()

        return reg_val

@dataclass
class RegisterSPI2VMMBank2:
    address_hex: int = 2
    L0offset_i: int = 0
    offset_i: int = 0

    def L0offset_i_field(self): return (self.L0offset_i & 0xFFF) << 20
    def offset_i_field(self): return (self.offset_i & 0xFFF) << 8

    def __index__(self):
        reg_val = 0
        reg_val |= \
            self.L0offset_i_field() | \
            self.offset_i_field()

        return reg_val

@dataclass
class VmmData:
    vmm_idx: int
    spi0_vmm_bank1: RegisterSPI0VMMBank1 = field(default_factory=make_RegisterSPI0VMMBank1)
    spi0_vmm_bank2: RegisterSPI0VMMBank2 = field(default_factory=make_RegisterSPI0VMMBank2)
    spi1_vmm_bank1: RegisterSPI1VMMBank1 = field(default_factory=make_RegisterSPI1VMMBank1)
    spi1_vmm_bank2: RegisterSPI1VMMBank2 = field(default_factory=make_RegisterSPI1VMMBank2)
    spi2_vmm_bank1: RegisterSPI2VMMBank1 = field(default_factory=make_RegisterSPI2VMMBank1)
    spi2_vmm_bank2: RegisterSPI2VMMBank2 = field(default_factory=make_RegisterSPI2VMMBank2)
    channel_settings: ChannelSettings = field(default_factory=make_default_channel_settings)


@dataclass
class VmmList:
    vmmList: List[VmmData] = field(default_factory=make_default_vmm_list)

    def get_as_list(self):
        return self.vmmList


@dataclass
class HybridData:
    hybrid_idx: int
    vmms: VmmList = field(default_factory=make_default_vmms_list)


@dataclass
class HybridList:
    hybridList: List[HybridData] = field(default_factory=make_default_hybrid_list)

    def get_as_list(self):
        return self.hybridList

    def append(self, data: HybridData):
        self.hybridList.append(data)


@dataclass
class FecData:
    fec_idx: int
    hybrids: HybridList = field(default_factory=make_default_hybrids_list)


@dataclass
class FecList:
    fecList: List[FecData] = field(default_factory=make_default_fec_list)

    def get_as_list(self):
        return self.fecList

    def append(self, data: FecData):
        self.fecList.append(data)

    def next_vmm(self, fec_idx: int = 0, hybrid_idx: int = 0, vmm_idx: int = 0):

        if fec_idx >= len(self.fecList):
            return
        fec_data = self.fecList[fec_idx]

        if hybrid_idx >= len(fec_data.hybrids.hybridList):
            yield from self.next_vmm(fec_idx + 1, 0, 0)
            return

        hybrid_data = fec_data.hybrids.hybridList[hybrid_idx]
        if vmm_idx >= len(hybrid_data.vmms.vmmList):
            yield from self.next_vmm(fec_idx, hybrid_idx + 1, 0)
            return

        yield hybrid_data.vmms.vmmList[vmm_idx], fec_data.fec_idx, hybrid_data.hybrid_idx

        yield from self.next_vmm(fec_idx, hybrid_idx, vmm_idx + 1)








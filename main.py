from PyQt5.QtCore import QThread, pyqtSignal

from ProgressWindow import ProgressWindow, MyWorker
from data.vmmsettingsdata import FecData, HybridData, VmmData
from generated_ui.SlowControlMain_lci import Ui_MainWindow
from PyQt5 import QtCore, QtWidgets
from FEC import FEC
from channels import ChannelsWidget

import os
import configurationhandler as handler


class SlowControlMainWindow(QtWidgets.QMainWindow):
    _currentWorkingDirectory = QtCore.QDir.currentPath()
    readConfigFile = pyqtSignal(str)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        self.connect_main_window_ports()
        self.progress_window = ProgressWindow()
        self.thread = QThread()
        self.pb_worker = MyWorker()
        self.pb_worker.moveToThread(self.thread)
        self.configuration = None

        self.readConfigFile.connect(self.pb_worker.read_config)

        self.pb_worker.beginConfigSig.connect(self.pb_worker.proc_config)
        self.pb_worker.intReady.connect(self.progress_window.on_count_changed)
        self.pb_worker.fecDataNext.connect(self.config_apply)
        self.pb_worker.hybridDataNext.connect(self.config_apply_hybrid)
        self.pb_worker.vmmDataNext.connect(self.config_apply_vmm)
        self.pb_worker.finished.connect(self.config_load_finished)

        self.thread.start()
        self.channels_widget = ChannelsWidget(self.ui.vmmSlowControl_tab)
        self.ui.horizontalLayout_2.addWidget(self.channels_widget)

        self.ui.FECs_tabWidget.insertTab(0, FEC(self.ui.FECs_tabWidget), "FEC_1")
        self.ui.FECs_tabWidget.insertTab(1, FEC(self.ui.FECs_tabWidget), "FEC_2")
        self.ui.FECs_tabWidget.insertTab(2, FEC(self.ui.FECs_tabWidget), "FEC_3")
        self.ui.FECs_tabWidget.insertTab(3, FEC(self.ui.FECs_tabWidget), "FEC_4")
        self.ui.FECs_tabWidget.insertTab(4, FEC(self.ui.FECs_tabWidget), "FEC_5")
        self.ui.FECs_tabWidget.insertTab(5, FEC(self.ui.FECs_tabWidget), "FEC_6")
        self.ui.FECs_tabWidget.insertTab(6, FEC(self.ui.FECs_tabWidget), "FEC_7")
        self.ui.FECs_tabWidget.insertTab(7, FEC(self.ui.FECs_tabWidget), "FEC_8")

        self.ui.numberOfCards_spinBox.setValue(1)

    def connect_main_window_ports(self):
        self.ui.numberOfCards_spinBox.valueChanged.connect(self.fec_number_changed)
        self.ui.saveConfigFile_pushButton.clicked.connect(self.config_save)
        self.ui.loadConfigFile_pushButton.clicked.connect(self.config_load)
        self.ui.FECs_tabWidget.tabBarClicked.connect(self.update_channel_settings)
        self.ui.FECs_tabWidget.currentChanged.connect(self.reload_channel_settings)
        self.ui.FECs_tabWidget.currentChanged.connect(self.update_acq)
        self.ui.send_pushButton.clicked.connect(lambda: self.pb_worker.send_config(self))
        self.ui.acqOnOffGlobal_pushButton.clicked.connect(self.acq_global_trigger)

    def get_active_idx(self):
        fec_idxl = self.ui.FECs_tabWidget.currentIndex()
        fec: FEC = self.ui.FECs_tabWidget.currentWidget()
        hybrid_idxl = fec.ui.hybrid_tabWidget.currentIndex()
        hybrid = fec.ui.hybrid_tabWidget.currentWidget()
        vmm_idxl = hybrid.ui.VMMs_tabWidget.currentIndex()

        return fec_idxl, hybrid_idxl, vmm_idxl

    def acq_global_trigger(self):
        for idx in range(0, self.ui.FECs_tabWidget.count()):
            fec: FEC = self.ui.FECs_tabWidget.widget(idx)
            fec.ui.acq_pushButton.setDisabled(self.ui.acqOnOffGlobal_pushButton.isChecked())
            fec.ui.acq_pushButton.setChecked(self.ui.acqOnOffGlobal_pushButton.isChecked())
            self.ui.send_pushButton.setDisabled(self.ui.acqOnOffGlobal_pushButton.isChecked())
            self.ui.send_pushButton.setFlat(self.ui.acqOnOffGlobal_pushButton.isChecked())

    def update_acq(self):
        fec: FEC = self.ui.FECs_tabWidget.currentWidget()
        self.ui.send_pushButton.setDisabled(fec.ui.acq_pushButton.isChecked())
        self.ui.send_pushButton.setFlat(fec.ui.acq_pushButton.isChecked())

    def update_channel_settings(self):
        self.channels_widget.update_channel_settings_in_current_vmm(self)

    def reload_channel_settings(self):
        fec = self.ui.FECs_tabWidget.currentWidget()
        if fec is None:
            self.channels_widget.hide()
            return
        fec.reload_channel_settings()

    def fec_number_changed(self):
        self.ui.FECs_tabWidget.setCurrentWidget(self.ui.FECs_tabWidget.widget(self.ui.numberOfCards_spinBox.value() - 1))
        for i in range(0, self.ui.numberOfCards_spinBox.value()):
            self.ui.FECs_tabWidget.setTabVisible(i, True)
        for i in range(self.ui.numberOfCards_spinBox.value(), 8):
            self.ui.FECs_tabWidget.setTabVisible(i, False)

    def config_save(self):
        self.update_channel_settings()
        default_config_file_name = 'configuration.json'
        filename, _ = QtWidgets.QFileDialog.getSaveFileName(self,
                                                            'Save Configuration to a File',
                                                            os.path.join(self._currentWorkingDirectory,
                                                                         default_config_file_name),
                                                            "JSON files (*.json);;all files(*.*)")
        if not filename:
            return

        handler.save_configuration_to_file(self.ui, filename)
        self._currentWorkingDirectory = os.path.dirname(filename)

    def config_load(self, default: bool = False):
        filename: str

        if not default:
            default_config_file_name = 'configuration.json'
            filename, _ = QtWidgets.QFileDialog.getOpenFileName(self,
                                                            'Load Configuration from File',
                                                            self._currentWorkingDirectory,
                                                            "JSON files (*.json);;all files(*.*)")
            if not filename:
                return
            self.update_channel_settings()
            self.channels_widget.setDisabled(True)
            self._currentWorkingDirectory = os.path.dirname(filename)
            self.progress_window.start_progress()
            self.progress_window.ui.label.setText("Reading Configuration")
            self.readConfigFile.emit(filename)
            self.progress_window.ui.label.setText("Applying Configuration")

        else:
            pass


    def config_load_finished(self):
        self.channels_widget.setEnabled(True)
        self.ui.FECs_tabWidget.currentWidget().reload_channel_settings()
        self.progress_window.fin_progress()

    def config_apply(self, fec_data: FecData):
        handler.apply_fec_configuration(self.ui, fec_data)
        # app.processEvents(QEventLoop.ExcludeUserInputEvents)

    def config_apply_hybrid(self, hybrid_data: HybridData, fec_idx: int):
        handler.apply_hybrid_configuration(self.ui, hybrid_data, fec_idx)
        # app.processEvents(QEventLoop.ExcludeUserInputEvents)

    def config_apply_vmm(self, vmm_data: VmmData, fec_idx: int, hybrid_idx: int):
        handler.apply_vmm_configuration(self.ui, vmm_data, fec_idx, hybrid_idx)
        app.processEvents()
        # app.processEvents(QEventLoop.ExcludeUserInputEvents)


if __name__ == "__main__":
    app = QtWidgets.QApplication([])
    mainWindow = SlowControlMainWindow()
    progress_window = ProgressWindow()
    progress_window.ui.label.setText(
        "VMM Slow Control application is loading. \n Please wait ...")

    progress_window.setWindowFlags(QtCore.Qt.SplashScreen | QtCore.Qt.FramelessWindowHint)
    progress_window.ui.progressBar.setValue(1)
    progress_window.show()

    for fec_idx in range(0, 8):
        progress_window.ui.progressBar.setValue((fec_idx + 1) * 10)
        mainWindow.ui.FECs_tabWidget.widget(fec_idx).apply_default_settings()
        mainWindow.ui.numberOfCards_spinBox.setValue(fec_idx + 1)
    progress_window.ui.progressBar.setValue(90)

    mainWindow.show()

    progress_window.close()
    app.exec_()

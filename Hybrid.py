from PyQt5 import QtWidgets
from VMM import VMM
from data.vmmsettingsdata import Channel
from generated_ui.hybrid_frame_lci import Ui_Form as HybridForm
import data.vmmsettingsdata as VmmData


class Hybrid(QtWidgets.QWidget):

    def __init__(self, parent=None):
        super().__init__(parent)
        self.ui = HybridForm()
        self.ui.setupUi(self)

        self.ui.VMM1_checkBox.stateChanged.connect(lambda: self.vmm_state_changed(1))
        self.ui.VMM2_checkBox.stateChanged.connect(lambda: self.vmm_state_changed(2))
        self.ui.apply_all_hybrids_pushButton.clicked.connect(self.apply_to_all_hybrids)
        self.ui.VMMs_tabWidget.tabBarClicked.connect(self.update_channel_settings)
        self.ui.VMMs_tabWidget.currentChanged.connect(self.reload_channel_settings)
        self.ui.read_I2C_pushButton.clicked.connect(self.read_I2C)

    def read_I2C(self):
        # TODO: reading I2C for current hybrid as in the code snippet below:
        #  QString result = m_fecWindow->m_daqWindow->m_daq.m_fecs[m_fecIndex].m_fecConfigModule->ReadI2C(m_hybridIndex, m_ui->cbChoiceI2C->currentIndex());
        result: str = "TODO"
        self.ui.result_I2C_textEdit.setText(result)

    def update_channel_settings(self):
        self.window().update_channel_settings()

    def reload_channel_settings(self):
        vmm = self.ui.VMMs_tabWidget.currentWidget()
        if vmm is None:
            main_window = self.window()
            main_window.channels_widget.hide()
            return
        import configurationhandler
        configurationhandler.set_vmm_channel_settings_to_ui(vmm)

    def apply_to_all_hybrids(self):
        import configurationhandler
        configurationhandler.set_hybrid_settings_to_all_hybrids(self)

    def create_vmm_tab(self, tab_idx):
        count = self.ui.VMMs_tabWidget.count()
        # create tabs if necessary, if there is no tabs and we need VMM 2, then VMM 1 will be added too
        for tab in range(count, tab_idx):
            vmm = VMM(self.ui.VMMs_tabWidget)
            self.ui.VMMs_tabWidget.insertTab(tab, vmm, "VMM_" + str(tab + 1))
            self.ui.VMMs_tabWidget.setTabVisible(tab, False)

    def vmm_state_changed(self, vmm_idx):

        is_checked = False
        if vmm_idx == 1:
            is_checked = self.ui.VMM1_checkBox.isChecked()
        elif vmm_idx == 2:
            is_checked = self.ui.VMM2_checkBox.isChecked()

        if is_checked:
            self.create_vmm_tab(vmm_idx)
            self.ui.VMMs_tabWidget.setTabVisible(vmm_idx - 1, True)
            self.ui.VMMs_tabWidget.setCurrentIndex(vmm_idx - 1)
        else:
            self.ui.VMMs_tabWidget.setTabVisible(vmm_idx - 1, False)

    def apply_default_settings(self):
        self.ui.VMM1_checkBox.setChecked(True)
        self.ui.VMM2_checkBox.setChecked(True)

    def apply_settings_from_other_hybrid(self, hybrid: "Hybrid"):
        self.ui.choice_I2C_comboBox.setCurrentIndex(
            hybrid.ui.choice_I2C_comboBox.currentIndex())

        self.ui.tp_skew_comboBox.setCurrentIndex(
            hybrid.ui.tp_skew_comboBox.currentIndex())

        self.ui.tp_width_comboBox.setCurrentIndex(
            hybrid.ui.tp_width_comboBox.currentIndex())

        self.ui.tp_polarity_comboBox.setCurrentIndex(
            hybrid.ui.tp_polarity_comboBox.currentIndex())

    def get_vmm(self, vmm_idx):
        return self.ui.VMMs_tabWidget.widget(vmm_idx - 1)

    def get_channel(self, vmm_idx, channel_idx):
        # return default channel data if VMMs is disabled
        if self.ui.VMMs_tabWidget.count() < (vmm_idx - 1):
            return VmmData.make_default_channel(channel_idx)
        vmm = self.get_vmm(vmm_idx)
        return vmm.get_channel(channel_idx)

    def update_channel(self, channel: Channel, vmm_idx: int):
        vmm = self.get_vmm(vmm_idx)
        vmm.update_channel(channel)

    def get_slvs(self, vmm_idx):
        return self.get_vmm(vmm_idx).get_slvs()

    def get_s32(self, vmm_idx):
        return self.get_vmm(vmm_idx).get_s32()

    def get_stcr(self, vmm_idx):
        return self.get_vmm(vmm_idx).get_stcr()

    def get_ssart(self, vmm_idx):
        return self.get_vmm(vmm_idx).get_ssart()

    def get_srec(self, vmm_idx):
        return self.get_vmm(vmm_idx).get_srec()

    def get_stlc(self, vmm_idx):
        return self.get_vmm(vmm_idx).get_stlc()

    def get_sbip(self, vmm_idx):
        return self.get_vmm(vmm_idx).get_sbip()

    def get_srat(self, vmm_idx):
        return self.get_vmm(vmm_idx).get_srat()

    def get_sfrst(self, vmm_idx):
        return self.get_vmm(vmm_idx).get_sfrst()

    def get_slvsbc(self, vmm_idx):
        return self.get_vmm(vmm_idx).get_slvsbc()

    def get_slvstp(self, vmm_idx):
        return self.get_vmm(vmm_idx).get_slvstp()

    def get_slvstk(self, vmm_idx):
        return self.get_vmm(vmm_idx).get_slvstk()

    def get_slvsdt(self, vmm_idx):
        return self.get_vmm(vmm_idx).get_slvsdt()

    def get_slvsart(self, vmm_idx):
        return self.get_vmm(vmm_idx).get_slvsart()

    def get_slvstki(self, vmm_idx):
        return self.get_vmm(vmm_idx).get_slvstki()

    def get_slvsena(self, vmm_idx):
        return self.get_vmm(vmm_idx).get_slvsena()

    def get_slvs6b(self, vmm_idx):
        return self.get_vmm(vmm_idx).get_slvs6b()

    def get_slh(self, vmm_idx):
        return self.get_vmm(vmm_idx).get_slh()

    def get_slxh(self, vmm_idx):
        return self.get_vmm(vmm_idx).get_slxh()

    def get_stgc(self, vmm_idx):
        return self.get_vmm(vmm_idx).get_stgc()

    def get_reset1(self, vmm_idx):
        return self.get_vmm(vmm_idx).get_reset1()

    def get_reset2(self, vmm_idx):
        return self.get_vmm(vmm_idx).get_reset2()

    def get_nskipm_i(self, vmm_idx):
        return self.get_vmm(vmm_idx).get_nskipm_i()

    def get_sdt_lower_6bits(self, vmm_idx):
        return self.get_vmm(vmm_idx).get_sdt_lower_6bits()

    def get_sdp10(self, vmm_idx):
        return self.get_vmm(vmm_idx).get_sdp10()

    def get_sc10b(self, vmm_idx):
        return self.get_vmm(vmm_idx).get_sc10b()

    def get_sc8b(self, vmm_idx):
        return self.get_vmm(vmm_idx).get_sc8b()

    def get_sc6b(self, vmm_idx):
        return self.get_vmm(vmm_idx).get_sc6b()

    def get_s8b(self, vmm_idx):
        return self.get_vmm(vmm_idx).get_s8b()

    def get_s6b(self, vmm_idx):
        return self.get_vmm(vmm_idx).get_s6b()

    def get_s10b(self, vmm_idx):
        return self.get_vmm(vmm_idx).get_s10b()

    def get_sdcks(self, vmm_idx):
        return self.get_vmm(vmm_idx).get_sdcks()

    def get_sdcka(self, vmm_idx):
        return self.get_vmm(vmm_idx).get_sdcka()

    def get_sdck6b(self, vmm_idx):
        return self.get_vmm(vmm_idx).get_sdck6b()

    def get_sdrv(self, vmm_idx):
        return self.get_vmm(vmm_idx).get_sdrv()

    def get_stpp(self, vmm_idx):
        return self.get_vmm(vmm_idx).get_stpp()

    def get_sL0cktest(self, vmm_idx):
        return self.get_vmm(vmm_idx).get_sL0cktest()

    def get_sL0dckinv(self, vmm_idx):
        return self.get_vmm(vmm_idx).get_sL0dckinv()

    def get_sL0ckinv(self, vmm_idx):
        return self.get_vmm(vmm_idx).get_sL0ckinv()

    def get_sL0ena(self, vmm_idx):
        return self.get_vmm(vmm_idx).get_sL0ena()

    def get_sL0enaV(self, vmm_idx):
        return self.get_vmm(vmm_idx).get_sL0enaV()

    def get_truncate_i(self, vmm_idx):
        return self.get_vmm(vmm_idx).get_truncate_i()

    def get_nskip_i(self, vmm_idx):
        return self.get_vmm(vmm_idx).get_nskip_i()

    def get_window_i(self, vmm_idx):
        return self.get_vmm(vmm_idx).get_window_i()

    def get_rollover_i(self, vmm_idx):
        return self.get_vmm(vmm_idx).get_rollover_i()

    def get_L0offset_i(self, vmm_idx):
        return self.get_vmm(vmm_idx).get_L0offset_i()

    def get_offset_i(self, vmm_idx):
        return self.get_vmm(vmm_idx).get_offset_i()

    def get_sp(self, vmm_idx):
        return self.get_vmm(vmm_idx).get_sp()

    def get_sdp(self, vmm_idx):
        return self.get_vmm(vmm_idx).get_sdp()

    def get_sbmx(self, vmm_idx):
        return self.get_vmm(vmm_idx).get_sbmx()

    def get_sbft(self, vmm_idx):
        return self.get_vmm(vmm_idx).get_sbft()

    def get_sbfp(self, vmm_idx):
        return self.get_vmm(vmm_idx).get_sbfp()

    def get_sbfm(self, vmm_idx):
        return self.get_vmm(vmm_idx).get_sbfm()

    def get_slg(self, vmm_idx):
        return self.get_vmm(vmm_idx).get_slg()

    def get_sm(self, vmm_idx):
        return self.get_vmm(vmm_idx).get_sm()

    def get_scmx(self, vmm_idx):
        return self.get_vmm(vmm_idx).get_scmx()

    def get_sfa(self, vmm_idx):
        return self.get_vmm(vmm_idx).get_sfa()

    def get_sfam(self, vmm_idx):
        return self.get_vmm(vmm_idx).get_sfam()

    def get_st(self, vmm_idx):
        return self.get_vmm(vmm_idx).get_st()

    def get_sfm(self, vmm_idx):
        return self.get_vmm(vmm_idx).get_sfm()

    def get_sg(self, vmm_idx):
        return self.get_vmm(vmm_idx).get_sg()

    def get_sng(self, vmm_idx):
        return self.get_vmm(vmm_idx).get_sng()

    def get_stot(self, vmm_idx):
        return self.get_vmm(vmm_idx).get_stot()

    def get_sttt(self, vmm_idx):
        return self.get_vmm(vmm_idx).get_sttt()

    def get_ssh(self, vmm_idx):
        return self.get_vmm(vmm_idx).get_ssh()

    def get_stc(self, vmm_idx):
        return self.get_vmm(vmm_idx).get_stc()

    def get_sdt_upper_4bits(self, vmm_idx):
        return self.get_vmm(vmm_idx).get_sdt_upper_4bits()

    def set_slvs(self, vmm_idx, value):
        self.get_vmm(vmm_idx).set_slvs(value)

    def set_s32(self, vmm_idx, value):
        self.get_vmm(vmm_idx).set_s32(value)

    def set_stcr(self, vmm_idx, value):
        self.get_vmm(vmm_idx).set_stcr(value)

    def set_ssart(self, vmm_idx, value):
        self.get_vmm(vmm_idx).set_ssart(value)

    def set_srec(self, vmm_idx, value):
        self.get_vmm(vmm_idx).set_srec(value)

    def set_stlc(self, vmm_idx, value):
        self.get_vmm(vmm_idx).set_stlc(value)

    def set_sbip(self, vmm_idx, value):
        self.get_vmm(vmm_idx).set_sbip(value)

    def set_srat(self, vmm_idx, value):
        self.get_vmm(vmm_idx).set_srat(value)

    def set_sfrst(self, vmm_idx, value):
        self.get_vmm(vmm_idx).set_sfrst(value)

    def set_slvsbc(self, vmm_idx, value):
        self.get_vmm(vmm_idx).set_slvsbc(value)

    def set_slvstp(self, vmm_idx, value):
        self.get_vmm(vmm_idx).set_slvstp(value)

    def set_slvstk(self, vmm_idx, value):
        self.get_vmm(vmm_idx).set_slvstk(value)

    def set_slvsdt(self, vmm_idx, value):
        self.get_vmm(vmm_idx).set_slvsdt(value)

    def set_slvsart(self, vmm_idx, value):
        self.get_vmm(vmm_idx).set_slvsart(value)

    def set_slvstki(self, vmm_idx, value):
        self.get_vmm(vmm_idx).set_slvstki(value)

    def set_slvsena(self, vmm_idx, value):
        self.get_vmm(vmm_idx).set_slvsena(value)

    def set_slvs6b(self, vmm_idx, value):
        self.get_vmm(vmm_idx).set_slvs6b(value)

    def set_slh(self, vmm_idx, value):
        self.get_vmm(vmm_idx).set_slh(value)

    def set_slxh(self, vmm_idx, value):
        self.get_vmm(vmm_idx).set_slxh(value)

    def set_stgc(self, vmm_idx, value):
        self.get_vmm(vmm_idx).set_stgc(value)

    def set_reset1(self, vmm_idx, value):
        self.get_vmm(vmm_idx).set_reset1(value)

    def set_reset2(self, vmm_idx, value):
        self.get_vmm(vmm_idx).set_reset2(value)

    def set_nskipm_i(self, vmm_idx, value):
        self.get_vmm(vmm_idx).set_nskipm_i(value)

    def set_sdt_lower_6bits(self, vmm_idx, value):
        self.get_vmm(vmm_idx).set_sdt_lower_6bits(value)

    def set_sdp10(self, vmm_idx, value):
        self.get_vmm(vmm_idx).set_sdp10(value)

    def set_sc10b(self, vmm_idx, value):
        self.get_vmm(vmm_idx).set_sc10b(value)

    def set_sc8b(self, vmm_idx, value):
        self.get_vmm(vmm_idx).set_sc8b(value)

    def set_sc6b(self, vmm_idx, value):
        self.get_vmm(vmm_idx).set_sc6b(value)

    def set_s8b(self, vmm_idx, value):
        self.get_vmm(vmm_idx).set_s8b(value)

    def set_s6b(self, vmm_idx, value):
        self.get_vmm(vmm_idx).set_s6b(value)

    def set_s10b(self, vmm_idx, value):
        self.get_vmm(vmm_idx).set_s10b(value)

    def set_sdcks(self, vmm_idx, value):
        self.get_vmm(vmm_idx).set_sdcks(value)

    def set_sdcka(self, vmm_idx, value):
        self.get_vmm(vmm_idx).set_sdcka(value)

    def set_sdck6b(self, vmm_idx, value):
        self.get_vmm(vmm_idx).set_sdck6b(value)

    def set_sdrv(self, vmm_idx, value):
        self.get_vmm(vmm_idx).set_sdrv(value)

    def set_stpp(self, vmm_idx, value):
        self.get_vmm(vmm_idx).set_stpp(value)

    def set_sL0cktest(self, vmm_idx, value):
        self.get_vmm(vmm_idx).set_sL0cktest(value)

    def set_sL0dckinv(self, vmm_idx, value):
        self.get_vmm(vmm_idx).set_sL0dckinv(value)

    def set_sL0ckinv(self, vmm_idx, value):
        self.get_vmm(vmm_idx).set_sL0ckinv(value)

    def set_sL0ena(self, vmm_idx, value):
        self.get_vmm(vmm_idx).set_sL0ena(value)

    def set_sL0enaV(self, vmm_idx, value):
        self.get_vmm(vmm_idx).set_sL0enaV(value)

    def set_truncate_i(self, vmm_idx, value):
        self.get_vmm(vmm_idx).set_truncate_i(value)

    def set_nskip_i(self, vmm_idx, value):
        self.get_vmm(vmm_idx).set_nskip_i(value)

    def set_window_i(self, vmm_idx, value):
        self.get_vmm(vmm_idx).set_window_i(value)

    def set_rollover_i(self, vmm_idx, value):
        self.get_vmm(vmm_idx).set_rollover_i(value)

    def set_L0offset_i(self, vmm_idx, value):
        self.get_vmm(vmm_idx).set_L0offset_i(value)

    def set_offset_i(self, vmm_idx, value):
        self.get_vmm(vmm_idx).set_offset_i(value)

    def set_sp(self, vmm_idx, value):
        self.get_vmm(vmm_idx).set_sp(value)

    def set_sdp(self, vmm_idx, value):
        self.get_vmm(vmm_idx).set_sdp(value)

    def set_sbmx(self, vmm_idx, value):
        self.get_vmm(vmm_idx).set_sbmx(value)

    def set_sbft(self, vmm_idx, value):
        self.get_vmm(vmm_idx).set_sbft(value)

    def set_sbfp(self, vmm_idx, value):
        self.get_vmm(vmm_idx).set_sbfp(value)

    def set_sbfm(self, vmm_idx, value):
        self.get_vmm(vmm_idx).set_sbfm(value)

    def set_slg(self, vmm_idx, value):
        self.get_vmm(vmm_idx).set_slg(value)

    def set_sm(self, vmm_idx, value):
        self.get_vmm(vmm_idx).set_sm(value)

    def set_scmx(self, vmm_idx, value):
        self.get_vmm(vmm_idx).set_scmx(value)

    def set_sfa(self, vmm_idx, value):
        self.get_vmm(vmm_idx).set_sfa(value)

    def set_sfam(self, vmm_idx, value):
        self.get_vmm(vmm_idx).set_sfam(value)

    def set_st(self, vmm_idx, value):
        self.get_vmm(vmm_idx).set_st(value)

    def set_sfm(self, vmm_idx, value):
        self.get_vmm(vmm_idx).set_sfm(value)

    def set_sg(self, vmm_idx, value):
        self.get_vmm(vmm_idx).set_sg(value)

    def set_sng(self, vmm_idx, value):
        self.get_vmm(vmm_idx).set_sng(value)

    def set_stot(self, vmm_idx, value):
        self.get_vmm(vmm_idx).set_stot(value)

    def set_sttt(self, vmm_idx, value):
        self.get_vmm(vmm_idx).set_sttt(value)

    def set_ssh(self, vmm_idx, value):
        self.get_vmm(vmm_idx).set_ssh(value)

    def set_stc(self, vmm_idx, value):
        self.get_vmm(vmm_idx).set_stc(value)

    def set_sdt_upper_4bits(self, vmm_idx, value):
        self.get_vmm(vmm_idx).set_sdt_upper_4bits(value)


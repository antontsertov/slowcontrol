# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'R:\workspace_ESS\repos\slowControl\ui\SlowControlMain.ui'
#
# Created by: PyQt5 UI code generator 5.15.4
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(1676, 925)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.centralwidget)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.global_tabWidget = QtWidgets.QTabWidget(self.centralwidget)
        self.global_tabWidget.setObjectName("global_tabWidget")
        self.vmmSlowControl_tab = QtWidgets.QWidget()
        self.vmmSlowControl_tab.setStyleSheet("")
        self.vmmSlowControl_tab.setObjectName("vmmSlowControl_tab")
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout(self.vmmSlowControl_tab)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.frame = QtWidgets.QFrame(self.vmmSlowControl_tab)
        self.frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame.setObjectName("frame")
        self.verticalLayout_3 = QtWidgets.QVBoxLayout(self.frame)
        self.verticalLayout_3.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.openCommunication_pushButton = QtWidgets.QPushButton(self.frame)
        self.openCommunication_pushButton.setMinimumSize(QtCore.QSize(0, 20))
        font = QtGui.QFont()
        font.setFamily("Arial Rounded MT Bold")
        self.openCommunication_pushButton.setFont(font)
        self.openCommunication_pushButton.setStyleSheet("QPushButton {\n"
"     background-color: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #565656, stop: 0.1 #525252, stop: 0.5 #4e4e4e, stop: 0.9 #4a4a4a, stop: 1 #464646);\n"
"    border-radius: 5;\n"
"    color: white;\n"
"}\n"
"\n"
"QPushButton:checked {    \n"
"    background-color: rgb(42, 168, 13);\n"
"}\n"
"\n"
"QPushButton:hover\n"
"{\n"
"    border: 1px solid yellow;\n"
"    color: yellow;\n"
"}")
        self.openCommunication_pushButton.setObjectName("openCommunication_pushButton")
        self.verticalLayout_3.addWidget(self.openCommunication_pushButton)
        self.communicationStatus_label = QtWidgets.QLabel(self.frame)
        self.communicationStatus_label.setAlignment(QtCore.Qt.AlignCenter)
        self.communicationStatus_label.setObjectName("communicationStatus_label")
        self.verticalLayout_3.addWidget(self.communicationStatus_label)
        self.send_pushButton = QtWidgets.QPushButton(self.frame)
        self.send_pushButton.setMinimumSize(QtCore.QSize(0, 20))
        font = QtGui.QFont()
        font.setFamily("Arial Rounded MT Bold")
        self.send_pushButton.setFont(font)
        self.send_pushButton.setStyleSheet("QPushButton {\n"
"     background-color: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #565656, stop: 0.1 #525252, stop: 0.5 #4e4e4e, stop: 0.9 #4a4a4a, stop: 1 #464646);\n"
"    border-radius: 5;\n"
"    color: white;\n"
"}\n"
"\n"
"QPushButton:checked {    \n"
"    background-color: rgb(42, 168, 13);\n"
"}\n"
"\n"
"QPushButton:hover\n"
"{\n"
"    border: 1px solid yellow;\n"
"    color: yellow;\n"
"}")
        self.send_pushButton.setObjectName("send_pushButton")
        self.verticalLayout_3.addWidget(self.send_pushButton)
        self.communicationWarnings_label = QtWidgets.QLabel(self.frame)
        self.communicationWarnings_label.setText("")
        self.communicationWarnings_label.setObjectName("communicationWarnings_label")
        self.verticalLayout_3.addWidget(self.communicationWarnings_label)
        self.resetWarnings_pushButton = QtWidgets.QPushButton(self.frame)
        self.resetWarnings_pushButton.setMinimumSize(QtCore.QSize(0, 20))
        font = QtGui.QFont()
        font.setFamily("Arial Rounded MT Bold")
        self.resetWarnings_pushButton.setFont(font)
        self.resetWarnings_pushButton.setStyleSheet("QPushButton {\n"
"     background-color: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #565656, stop: 0.1 #525252, stop: 0.5 #4e4e4e, stop: 0.9 #4a4a4a, stop: 1 #464646);\n"
"    border-radius: 5;\n"
"    color: white;\n"
"}\n"
"\n"
"QPushButton:checked {    \n"
"    background-color: rgb(42, 168, 13);\n"
"}\n"
"\n"
"QPushButton:hover\n"
"{\n"
"    border: 1px solid yellow;\n"
"    color: yellow;\n"
"}")
        self.resetWarnings_pushButton.setObjectName("resetWarnings_pushButton")
        self.verticalLayout_3.addWidget(self.resetWarnings_pushButton)
        spacerItem = QtWidgets.QSpacerItem(20, 20, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Fixed)
        self.verticalLayout_3.addItem(spacerItem)
        self.fec_groupBox = QtWidgets.QGroupBox(self.frame)
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.fec_groupBox.setFont(font)
        self.fec_groupBox.setFlat(True)
        self.fec_groupBox.setObjectName("fec_groupBox")
        self.verticalLayout_4 = QtWidgets.QVBoxLayout(self.fec_groupBox)
        self.verticalLayout_4.setObjectName("verticalLayout_4")
        self.numberOfCards_label = QtWidgets.QLabel(self.fec_groupBox)
        font = QtGui.QFont()
        font.setBold(False)
        font.setWeight(50)
        self.numberOfCards_label.setFont(font)
        self.numberOfCards_label.setObjectName("numberOfCards_label")
        self.verticalLayout_4.addWidget(self.numberOfCards_label)
        self.numberOfCards_spinBox = QtWidgets.QSpinBox(self.fec_groupBox)
        self.numberOfCards_spinBox.setStyleSheet("QSpinBox\n"
"{\n"
"    subcontrol-origin: padding;\n"
"    subcontrol-position: top right;\n"
"    selection-background-color: #111;\n"
"    selection-color: yellow;\n"
"    color: white;\n"
"    background-color: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #565656, stop: 0.1 #525252, stop: 0.5 #4e4e4e, stop: 0.9 #4a4a4a, stop: 1 #464646);\n"
"    border-style: solid;\n"
"    border: 1px solid #1e1e1e;\n"
"    border-radius: 5;\n"
"    padding: 1px 0px 1px 5px;\n"
"}\n"
"\n"
"\n"
"QSpinBox:hover, QPushButton:hover\n"
"{\n"
"    border: 1px solid yellow;\n"
"    color: white;\n"
"}\n"
"\n"
"\n"
"QSpinBox QAbstractItemView\n"
"{\n"
"    border: 2px solid darkgray;\n"
"    color: black;\n"
"    selection-background-color: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #111, stop: 1 #333);\n"
"}\n"
"/*UP arrow*/\n"
"QSpinBox::up-arrow \n"
"{\n"
"    border-left: 5px solid none;\n"
"    border-right: 5px solid none; \n"
"    border-bottom: 5px solid rgb(42, 168, 13); \n"
"    width: 0px; \n"
"    height: 0px; \n"
"}\n"
"\n"
"QSpinBox::up-arrow:hover \n"
"{ \n"
"    border-left: 5px solid none;\n"
"    border-right: 5px solid none; \n"
"    border-bottom: 10px solid yellow; \n"
"    width: 0px; \n"
"    height: 0px; \n"
"}\n"
"\n"
"QSpinBox::up-button \n"
"{\n"
"    border-radius: 5;\n"
"    width: 15px; \n"
"    height: 10px; \n"
"    background-color:  QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #565656, stop: 0.1 #525252, stop: 0.5 #4e4e4e, stop: 0.9 #4a4a4a, stop: 1 #464646);\n"
"}\n"
"\n"
"QSpinBox::up-button:hover \n"
"{\n"
"    width: 15px; \n"
"    height: 5px; \n"
"    background-color: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #565656, stop: 0.1 #525252, stop: 0.5 #4e4e4e, stop: 0.9 #4a4a4a, stop: 1 #464646); \n"
"}\n"
" \n"
"/*DOWN arrow*/\n"
"QSpinBox::down-arrow \n"
"{\n"
"    border-left: 5px solid none;\n"
"    border-right: 5px solid none; \n"
"    border-top: 5px solid rgb(42, 168, 13); \n"
"    width: 0px; \n"
"    height: 0px; \n"
"}\n"
"\n"
"QSpinBox::down-arrow:hover \n"
"{\n"
"    border-left: 5px solid none;\n"
"    border-right: 5px solid none; \n"
"    border-top: 10px solid yellow;\n"
"    width: 0px; \n"
"    height: 0px;\n"
"}\n"
"\n"
"QSpinBox::down-button \n"
"{\n"
"    border-radius: 5;\n"
"    width: 15px; \n"
"    height: 10px; \n"
"    background-color:  QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #565656, stop: 0.1 #525252, stop: 0.5 #4e4e4e, stop: 0.9 #4a4a4a, stop: 1 #464646);\n"
"}\n"
"QSpinBox::down-button:hover \n"
"{\n"
"    width: 15px;\n"
"    height: 5px;\n"
"    background-color:  QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #565656, stop: 0.1 #525252, stop: 0.5 #4e4e4e, stop: 0.9 #4a4a4a, stop: 1 #464646);\n"
"}")
        self.numberOfCards_spinBox.setMinimum(1)
        self.numberOfCards_spinBox.setMaximum(8)
        self.numberOfCards_spinBox.setObjectName("numberOfCards_spinBox")
        self.verticalLayout_4.addWidget(self.numberOfCards_spinBox)
        self.clock_label = QtWidgets.QLabel(self.fec_groupBox)
        font = QtGui.QFont()
        font.setBold(False)
        font.setWeight(50)
        self.clock_label.setFont(font)
        self.clock_label.setObjectName("clock_label")
        self.verticalLayout_4.addWidget(self.clock_label)
        self.clockSource_comboBox = QtWidgets.QComboBox(self.fec_groupBox)
        self.clockSource_comboBox.setStyleSheet("QComboBox\n"
"{\n"
"    subcontrol-origin: padding;\n"
"    subcontrol-position: top right;\n"
"    selection-background-color: #111;\n"
"    selection-color: yellow;\n"
"    color: white;\n"
"    background-color: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #565656, stop: 0.1 #525252, stop: 0.5 #4e4e4e, stop: 0.9 #4a4a4a, stop: 1 #464646);\n"
"    border-style: solid;\n"
"    border: 1px solid #1e1e1e;\n"
"    border-radius: 5;\n"
"    padding: 1px 0px 1px 5px;\n"
"}\n"
"\n"
"\n"
"QComboBox:hover, QPushButton:hover\n"
"{\n"
"    border: 1px solid yellow;\n"
"    color: white;\n"
"}\n"
"\n"
"\n"
"\n"
"QComboBox:on\n"
"{\n"
"    padding-top: 0px;\n"
"    padding-left: 5px;\n"
"    color: white;\n"
"    background-color: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #2d2d2d, stop: 0.1 #2b2b2b, stop: 0.5 #292929, stop: 0.9 #282828, stop: 1 #252525);\n"
"    selection-background-color: #ffaa00;\n"
"}\n"
"\n"
"QComboBox:!on\n"
"{\n"
"    color: white;\n"
"    background-color: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #666, stop: 0.1 #555, stop: 0.5 #555, stop: 0.9 #444, stop: 1 #333);\n"
"}\n"
"\n"
"QComboBox QAbstractItemView\n"
"{\n"
"    border: 2px solid darkgray;\n"
"    color: black;\n"
"    selection-background-color: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #111, stop: 1 #333);\n"
"}\n"
"\n"
"QComboBox::drop-down\n"
"{\n"
"     subcontrol-origin: padding;\n"
"     subcontrol-position: top right;\n"
"     width: 5px;\n"
"     color: white;\n"
"     border-left-width: 0px;\n"
"     border-left-color: darkgray;\n"
"     border-left-style: solid; /* just a single line */\n"
"     border-top-right-radius: 3px; /* same radius as the QComboBox */\n"
"     border-bottom-right-radius: 3px;\n"
"     padding-left: 10px;\n"
" }\n"
"\n"
"")
        self.clockSource_comboBox.setObjectName("clockSource_comboBox")
        self.clockSource_comboBox.addItem("")
        self.clockSource_comboBox.addItem("")
        self.clockSource_comboBox.addItem("")
        self.clockSource_comboBox.addItem("")
        self.verticalLayout_4.addWidget(self.clockSource_comboBox)
        self.checkBox = QtWidgets.QCheckBox(self.fec_groupBox)
        font = QtGui.QFont()
        font.setBold(False)
        font.setWeight(50)
        self.checkBox.setFont(font)
        self.checkBox.setObjectName("checkBox")
        self.verticalLayout_4.addWidget(self.checkBox)
        self.verticalLayout_3.addWidget(self.fec_groupBox)
        spacerItem1 = QtWidgets.QSpacerItem(20, 207, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Fixed)
        self.verticalLayout_3.addItem(spacerItem1)
        self.acq_groupBox = QtWidgets.QGroupBox(self.frame)
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.acq_groupBox.setFont(font)
        self.acq_groupBox.setFlat(True)
        self.acq_groupBox.setObjectName("acq_groupBox")
        self.verticalLayout_5 = QtWidgets.QVBoxLayout(self.acq_groupBox)
        self.verticalLayout_5.setContentsMargins(0, -1, 0, 0)
        self.verticalLayout_5.setObjectName("verticalLayout_5")
        self.acqOnOffGlobal_pushButton = QtWidgets.QPushButton(self.acq_groupBox)
        self.acqOnOffGlobal_pushButton.setMinimumSize(QtCore.QSize(0, 20))
        self.acqOnOffGlobal_pushButton.setStyleSheet("QPushButton {\n"
"     background-color: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #565656, stop: 0.1 #525252, stop: 0.5 #4e4e4e, stop: 0.9 #4a4a4a, stop: 1 #464646);\n"
"    border-radius: 5;\n"
"    color: white;\n"
"}\n"
"\n"
"QPushButton:checked {    \n"
"    background-color: rgb(42, 168, 13);\n"
"}\n"
"\n"
"QPushButton:hover\n"
"{\n"
"    border: 1px solid yellow;\n"
"    color: yellow;\n"
"}")
        self.acqOnOffGlobal_pushButton.setCheckable(True)
        self.acqOnOffGlobal_pushButton.setObjectName("acqOnOffGlobal_pushButton")
        self.verticalLayout_5.addWidget(self.acqOnOffGlobal_pushButton)
        self.verticalLayout_3.addWidget(self.acq_groupBox)
        spacerItem2 = QtWidgets.QSpacerItem(20, 50, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Fixed)
        self.verticalLayout_3.addItem(spacerItem2)
        self.configFile_groupBox = QtWidgets.QGroupBox(self.frame)
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.configFile_groupBox.setFont(font)
        self.configFile_groupBox.setFlat(True)
        self.configFile_groupBox.setObjectName("configFile_groupBox")
        self.verticalLayout_6 = QtWidgets.QVBoxLayout(self.configFile_groupBox)
        self.verticalLayout_6.setContentsMargins(0, 10, 0, 9)
        self.verticalLayout_6.setObjectName("verticalLayout_6")
        self.configFile_lineEdit = QtWidgets.QLineEdit(self.configFile_groupBox)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.configFile_lineEdit.sizePolicy().hasHeightForWidth())
        self.configFile_lineEdit.setSizePolicy(sizePolicy)
        self.configFile_lineEdit.setMinimumSize(QtCore.QSize(100, 0))
        font = QtGui.QFont()
        font.setBold(False)
        font.setWeight(50)
        self.configFile_lineEdit.setFont(font)
        self.configFile_lineEdit.setObjectName("configFile_lineEdit")
        self.verticalLayout_6.addWidget(self.configFile_lineEdit)
        self.openConfigFile_pushButton = QtWidgets.QPushButton(self.configFile_groupBox)
        self.openConfigFile_pushButton.setMinimumSize(QtCore.QSize(0, 20))
        self.openConfigFile_pushButton.setStyleSheet("QPushButton {\n"
"     background-color: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #565656, stop: 0.1 #525252, stop: 0.5 #4e4e4e, stop: 0.9 #4a4a4a, stop: 1 #464646);\n"
"    border-radius: 5;\n"
"    color: white;\n"
"}\n"
"\n"
"QPushButton:checked {    \n"
"    background-color: rgb(42, 168, 13);\n"
"}\n"
"\n"
"QPushButton:hover\n"
"{\n"
"    border: 1px solid yellow;\n"
"    color: yellow;\n"
"}")
        self.openConfigFile_pushButton.setObjectName("openConfigFile_pushButton")
        self.verticalLayout_6.addWidget(self.openConfigFile_pushButton)
        self.loadConfigFile_pushButton = QtWidgets.QPushButton(self.configFile_groupBox)
        self.loadConfigFile_pushButton.setMinimumSize(QtCore.QSize(0, 20))
        self.loadConfigFile_pushButton.setStyleSheet("QPushButton {\n"
"     background-color: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #565656, stop: 0.1 #525252, stop: 0.5 #4e4e4e, stop: 0.9 #4a4a4a, stop: 1 #464646);\n"
"    border-radius: 5;\n"
"    color: white;\n"
"}\n"
"\n"
"QPushButton:checked {    \n"
"    background-color: rgb(42, 168, 13);\n"
"}\n"
"\n"
"QPushButton:hover\n"
"{\n"
"    border: 1px solid yellow;\n"
"    color: yellow;\n"
"}")
        self.loadConfigFile_pushButton.setObjectName("loadConfigFile_pushButton")
        self.verticalLayout_6.addWidget(self.loadConfigFile_pushButton)
        self.saveConfigFile_pushButton = QtWidgets.QPushButton(self.configFile_groupBox)
        self.saveConfigFile_pushButton.setMinimumSize(QtCore.QSize(0, 20))
        self.saveConfigFile_pushButton.setStyleSheet("QPushButton {\n"
"     background-color: QLinearGradient( x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #565656, stop: 0.1 #525252, stop: 0.5 #4e4e4e, stop: 0.9 #4a4a4a, stop: 1 #464646);\n"
"    border-radius: 5;\n"
"    color: white;\n"
"}\n"
"\n"
"QPushButton:checked {    \n"
"    background-color: rgb(42, 168, 13);\n"
"}\n"
"\n"
"QPushButton:hover\n"
"{\n"
"    border: 1px solid yellow;\n"
"    color: yellow;\n"
"}")
        self.saveConfigFile_pushButton.setObjectName("saveConfigFile_pushButton")
        self.verticalLayout_6.addWidget(self.saveConfigFile_pushButton)
        spacerItem3 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout_6.addItem(spacerItem3)
        self.verticalLayout_3.addWidget(self.configFile_groupBox)
        self.horizontalLayout_2.addWidget(self.frame)
        self.FECs_tabWidget = QtWidgets.QTabWidget(self.vmmSlowControl_tab)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.FECs_tabWidget.sizePolicy().hasHeightForWidth())
        self.FECs_tabWidget.setSizePolicy(sizePolicy)
        self.FECs_tabWidget.setMinimumSize(QtCore.QSize(1300, 0))
        self.FECs_tabWidget.setObjectName("FECs_tabWidget")
        self.horizontalLayout_2.addWidget(self.FECs_tabWidget)
        spacerItem4 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem4)
        self.global_tabWidget.addTab(self.vmmSlowControl_tab, "")
        self.calibration_tab = QtWidgets.QWidget()
        self.calibration_tab.setObjectName("calibration_tab")
        self.global_tabWidget.addTab(self.calibration_tab, "")
        self.horizontalLayout.addWidget(self.global_tabWidget)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 1676, 21))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        self.global_tabWidget.setCurrentIndex(0)
        self.FECs_tabWidget.setCurrentIndex(-1)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.openCommunication_pushButton.setText(_translate("MainWindow", "Open Communication"))
        self.communicationStatus_label.setText(_translate("MainWindow", "N/A"))
        self.send_pushButton.setText(_translate("MainWindow", "Send"))
        self.resetWarnings_pushButton.setText(_translate("MainWindow", "Reset Warnings"))
        self.fec_groupBox.setTitle(_translate("MainWindow", "Assister"))
        self.numberOfCards_label.setText(_translate("MainWindow", "Number of cards"))
        self.clock_label.setText(_translate("MainWindow", "Clock"))
        self.clockSource_comboBox.setItemText(0, _translate("MainWindow", "ESS 88.0525 MHz"))
        self.clockSource_comboBox.setItemText(1, _translate("MainWindow", "board 88.8888 MHz"))
        self.clockSource_comboBox.setItemText(2, _translate("MainWindow", "SRS 44.4444 MHz"))
        self.clockSource_comboBox.setItemText(3, _translate("MainWindow", "SRS 40.0 MHz"))
        self.checkBox.setText(_translate("MainWindow", "Config check"))
        self.acq_groupBox.setTitle(_translate("MainWindow", "ACQ (all Assisters)"))
        self.acqOnOffGlobal_pushButton.setText(_translate("MainWindow", "ACQ"))
        self.configFile_groupBox.setTitle(_translate("MainWindow", "Configuration File"))
        self.openConfigFile_pushButton.setText(_translate("MainWindow", "Open File"))
        self.loadConfigFile_pushButton.setText(_translate("MainWindow", "Load"))
        self.saveConfigFile_pushButton.setText(_translate("MainWindow", "Save"))
        self.global_tabWidget.setTabText(self.global_tabWidget.indexOf(self.vmmSlowControl_tab), _translate("MainWindow", "VMM Slow Control"))
        self.global_tabWidget.setTabText(self.global_tabWidget.indexOf(self.calibration_tab), _translate("MainWindow", "Calibration"))

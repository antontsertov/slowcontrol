from PyQt5 import QtWidgets

from generated_ui.vmm_channel import Ui_Form as ChannelForm

options = (
    "0 mV",
    "1 mV",
    "2 mV",
    "3 mV",
    "4 mV",
    "5 mV",
    "6 mV",
    "7 mV",
    "8 mV",
    "9 mV",
    "10 mV",
    "11 mV",
    "12 mV",
    "13 mV",
    "14 mV",
    "15 mV",
    "16 mV",
    "17 mV",
    "18 mV",
    "19 mV",
    "20 mV",
    "21 mV",
    "22 mV",
    "23 mV",
    "24 mV",
    "25 mV",
    "26 mV",
    "27 mV",
    "28 mV",
    "29 mV",
    "30 mV",
    "31 mV")


class ChannelSettings(QtWidgets.QWidget):
    is_populated = False

    def __init__(self, parent=None):
        super().__init__(parent)
        self.ui = ChannelForm()
        self.ui.setupUi(self)
        self.populate_combos()
        self.ui.idx_pushButton.clicked.connect(self.populate_combos)

    def set_index(self, idx: int):
        self.ui.idx_pushButton.setText(str(idx))

    def populate_combos(self):
        if not self.is_populated:
            self.set_sd_values()
            self.set_sz010b_values()
            self.set_sz08b_values()
            self.set_sz06b_values()
            self.is_populated = True

    def set_sd_values(self):
        if self.ui.sd_comboBox.count() < 31:
            self.ui.sd_comboBox.addItems(options[1:])

    def set_sz010b_values(self):
        if self.ui.sz010b_comboBox.count() < 31:
            self.ui.sz010b_comboBox.addItems(options[1:])

    def set_sz08b_values(self):
        if self.ui.sz08b_comboBox.count() < 16:
            self.ui.sz08b_comboBox.addItems(options[1:16])

    def set_sz06b_values(self):
        if self.ui.sz06b_comboBox.count() < 8:
            self.ui.sz06b_comboBox.addItems(options[1:8])

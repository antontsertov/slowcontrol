from typing import List
from PyQt5 import QtWidgets
from channel import ChannelSettings
from data.vmmsettingsdata import Channel, make_default_channel_settings
from generated_ui.vmm_frame_lci import Ui_Form as VmmForm


class VMM(QtWidgets.QWidget):
    sc_state = False
    sl_state = False
    st_state = True
    sth_state = False
    sm_state = False
    smx_state = False
    sd_index = 0
    sz010b_index = 0
    sz08b_index = 0
    sz06b_index = 0

    def __init__(self, parent=None):
        super().__init__(parent)
        self.ui = VmmForm()
        self.ui.setupUi(self)

        self.ui.set_global_settings_pushButton.clicked.connect(self.set_global_settings)
        self.ui.set_channel_settings__pushButton.clicked.connect(self.set_channel_settings)
        self.ui.hard_reset_vmm_pushButton.clicked.connect(self.hard_reset_vmm)
        self.ui.hard_reset_all_pushButton.clicked.connect(self.hard_reset_all_vmm)
        self.channel_settings = make_default_channel_settings()

    def get_channel(self, idx):
        return self.channel_settings.get_channel(idx)

    def get_channels(self):
        return self.channel_settings.channels

    def update_channel_settings(self, channels: List[Channel]):
        for channel in channels:
            self.update_channel(channel)

    def update_channel(self, channel: Channel):
        self.channel_settings.update_channel(channel)

    def set_channel_settings_data(self, chnl_settings: ChannelSettings):
        self.channel_settings = chnl_settings

    # sets VMM (current) settings to every other VMM. Channel settings not included
    def set_global_settings(self):
        import configurationhandler
        configurationhandler.sync_channel_settings(self)
        configurationhandler.set_vmm_settings_to_all_vmms(self, configurationhandler.VmmConfigurationType.ALL_GLOBAL)

    # sets VMM (current) settings to every other VMM. Global settings not included
    def set_channel_settings(self):
        import configurationhandler
        configurationhandler.sync_channel_settings(self)
        configurationhandler.set_vmm_settings_to_all_vmms(self, configurationhandler.VmmConfigurationType.ALL_CHANNEL)

    def hard_reset_vmm(self):
        import configurationhandler
        configurationhandler.hard_reset_vmm(self)
        configurationhandler.set_vmm_channel_settings_to_ui(self)

    def hard_reset_all_vmm(self):
        import configurationhandler
        configurationhandler.hard_reset_all_vmm(self)
        configurationhandler.set_vmm_channel_settings_to_ui(self)

    def get_slvs(self):
        return 0  # Not used:  int(self.ui.slvs_pushButton.isChecked())

    def get_s32(self):
        return int(self.ui.s32_pushButton.isChecked())

    def get_stcr(self):
        return int(self.ui.stcr_pushButton.isChecked())

    def get_ssart(self):
        return int(self.ui.ssart_pushButton.isChecked())

    def get_srec(self):
        return int(self.ui.srec_pushButton.isChecked())

    def get_stlc(self):
        return int(self.ui.stlc_pushButton.isChecked())

    def get_sbip(self):
        return int(self.ui.sbip_pushButton.isChecked())

    def get_srat(self):
        return int(self.ui.sratMode_comboBox.currentIndex())

    def get_sfrst(self):
        return 0  # Not used:  int(self.ui.sfrst_pushButton.isChecked())

    def get_slvsbc(self):
        return int(self.ui.bc_pushButton.isChecked())

    def get_slvstp(self):
        return int(self.ui.tp_pushButton.isChecked())

    def get_slvstk(self):
        return int(self.ui.tk_pushButton.isChecked())

    def get_slvsdt(self):
        return int(self.ui.dt_pushButton.isChecked())

    def get_slvsart(self):
        return int(self.ui.art_pushButton.isChecked())

    def get_slvstki(self):
        return int(self.ui.tki_pushButton.isChecked())

    def get_slvsena(self):
        return int(self.ui.ena_pushButton.isChecked())

    def get_slvs6b(self):
        return int(self.ui.sixb_pushButton.isChecked())

    def get_slh(self):
        return int(self.ui.slh_pushButton.isChecked())

    def get_slxh(self):
        return int(self.ui.slxh_pushButton.isChecked())

    def get_stgc(self):
        return int(self.ui.stgc_pushButton.isChecked())

    def get_reset1(self):
        # TODO: unknwonn which UI object is responsible for RESET1
        return 0  # int(self.ui..isChecked())

    def get_reset2(self):
        # TODO: unknwonn which UI object is responsible for RESET2
        return 0  # int(self.ui..isChecked())

    def get_nskipm_i(self):
        return int(self.ui.nskipm_i_pushButton.isChecked())

    def get_sdt_lower_6bits(self):
        sdt = self.ui.thresholdDac_spinBox.value()
        return sdt & 0x3F

    def get_sdp10(self):
        return self.ui.testPulseDac_spinBox.value()

    def get_sc10b(self):
        return self.ui.tenBitAdc_comboBox.currentIndex()

    def get_sc8b(self):
        return self.ui.eightBitAdc_comboBox.currentIndex()

    def get_sc6b(self):
        return 0  # Not used:  self.ui.sixBitAdc_comboBox.currentIndex()

    def get_s8b(self):
        return int(self.ui.eightBitConvMode_pushButton.isChecked())

    def get_s6b(self):
        return 0  # Not used:  int(self.ui.sixBit_pushButton.isChecked())

    def get_s10b(self):
        return int(self.ui.adcOnOff_pushButton.isChecked())

    def get_sdcks(self):
        return int(self.ui.dualClockData_pushButton.isChecked())

    def get_sdcka(self):
        return int(self.ui.dualClockArt_pushButton.isChecked())

    def get_sdck6b(self):
        return 0  # Not used:  int(self.ui.dualClockSixBit_pushButton.isChecked())

    def get_sdrv(self):
        return 0  # Not used:  int(self.ui.analogTristates_pushButton.isChecked())

    def get_stpp(self):
        return  0  # Not used:

    def get_sL0dckinv(self):
        return int(self.ui.sL0dckinv_pushButton.isChecked())

    def get_sL0ckinv(self):
        return int(self.ui.sL0ckinv_pushButton.isChecked())

    def get_sp(self):
        return self.ui.inputChargePolarity_comboBox.currentIndex()

    def get_sdp(self):
        return int(self.ui.disableAtPeak_pushButton.isChecked())

    def get_sbmx(self):
        return int(self.ui.sbmx_pushButton.isChecked())

    def get_sbft(self):
        return int(self.ui.sbft_pushButton.isChecked())

    def get_sbfp(self):
        return int(self.ui.sbfp_pushButton.isChecked())

    def get_sbfm(self):
        return int(self.ui.sbfm_pushButton.isChecked())

    def get_slg(self):
        return int(self.ui.leakCurr_pushButton.isChecked())

    def get_sm(self):
        return self.ui.analogMonitor_comboBox.currentIndex()

    # TODO: verify that SCMX is controlled by ReadADC pushButton
    def get_scmx(self):
        return int(self.ui.readADC_pushButton.isChecked())

    def get_sfa(self):
        return self.ui.artMode_comboBox.currentIndex()

    def get_sfam(self):
        return self.ui.artMode_comboBox.currentIndex()

    def get_st(self):
        return self.ui.peakTime_comboBox.currentIndex()

    def get_sfm(self):
        return int(self.ui.doubleLeak_pushButton.isChecked())

    def get_sg(self):
        return self.ui.gain_comboBox.currentIndex()

    def get_sng(self):
        return int(self.ui.neighborTrigger_pushButton.isChecked())

    def get_stot(self):
        return 0  # Not used:  self.ui.modeStot_comboBox.currentIndex()

    def get_sttt(self): # NOTE modeled as STPP, not sure if this is correct
        return 0  # Not used:  self.ui.modeStpp_comboBox.currentIndex()

    def get_ssh(self):
        return int(self.ui.subHysterisis_pushButton.isChecked())

    def get_stc(self):
        return self.ui.tacSlopAdj_comboBox.currentIndex()

    def get_sdt_upper_4bits(self):
        sdt = self.ui.thresholdDac_spinBox.value()
        sdt = sdt & 0x3C0
        return sdt >> 6

    def get_sL0cktest(self):
        return int(self.ui.sL0cktest_pushButton.isChecked())

    def get_sL0ena(self):
        return int(self.ui.sL0ena_pushButton.isChecked())

    def get_sL0enaV(self):
        return int(self.ui.sL0enaV_pushButton.isChecked())

    def get_truncate_i(self):
        return self.ui.truncate_spinBox.value()

    def get_nskip_i(self):
        return self.ui.nskip_spinBox.value()

    def get_window_i(self):
        return self.ui.window_spinBox.value()

    def get_rollover_i(self):
        return self.ui.rollover_spinBox.value()

    def get_L0offset_i(self):
        return self.ui.l0offset_spinBox.value()

    def get_offset_i(self):
        return self.ui.offset_spinBox.value()

    # *****************************************************************
    def set_slvs(self, value: bool):
        pass # Not used:  int(self.ui.slvs_pushButton.isChecked())

    def set_s32(self, value: bool):
        self.ui.s32_pushButton.setChecked(value)

    def set_stcr(self, value: bool):
        self.ui.stcr_pushButton.setChecked(value)

    def set_ssart(self, value: bool):
        self.ui.ssart_pushButton.setChecked(value)

    def set_srec(self, value: bool):
        self.ui.srec_pushButton.setChecked(value)

    def set_stlc(self, value: bool):
        self.ui.stlc_pushButton.setChecked(value)

    def set_sbip(self, value: bool):
        self.ui.sbip_pushButton.setChecked(value)

    def set_srat(self, value: bool):
        self.ui.sratMode_comboBox.currentIndex()

    def set_sfrst(self, value: bool):
        pass  # Not used:  int(self.ui.sfrst_pushButton.setChecked(value)

    def set_slvsbc(self, value: bool):
        self.ui.bc_pushButton.setChecked(value)

    def set_slvstp(self, value: bool):
        self.ui.tp_pushButton.setChecked(value)

    def set_slvstk(self, value: bool):
        self.ui.tk_pushButton.setChecked(value)

    def set_slvsdt(self, value: bool):
        self.ui.dt_pushButton.setChecked(value)

    def set_slvsart(self, value: bool):
        self.ui.art_pushButton.setChecked(value)

    def set_slvstki(self, value: bool):
        self.ui.tki_pushButton.setChecked(value)

    def set_slvsena(self, value: bool):
        self.ui.ena_pushButton.setChecked(value)

    def set_slvs6b(self, value: bool):
        self.ui.sixb_pushButton.setChecked(value)

    def set_slh(self, value: bool):
        self.ui.slh_pushButton.setChecked(value)

    def set_slxh(self, value: bool):
        self.ui.slxh_pushButton.setChecked(value)

    def set_stgc(self, value: bool):
        self.ui.stgc_pushButton.setChecked(value)

    def set_reset1(self, value: bool):
        # TODO: unknwonn which UI object is responsible for RESET1
        pass # int(self.ui..setChecked(value)

    def set_reset2(self, value: bool):
        # TODO: unknwonn which UI object is responsible for RESET2
        pass #  int(self.ui..setChecked(value)

    def set_nskipm_i(self, value: bool):
        self.ui.nskipm_i_pushButton.setChecked(value)

    def set_sdt_lower_6bits(self, value):
        self.ui.thresholdDac_spinBox.setValue(value & 0x3F)

    def set_sdp10(self, value):
        self.ui.testPulseDac_spinBox.setValue(value)

    def set_sc10b(self, value):
        self.ui.tenBitAdc_comboBox.setCurrentIndex(value)

    def set_sc8b(self, value):
        self.ui.eightBitAdc_comboBox.setCurrentIndex(value)

    def set_sc6b(self, value: bool):
        pass  # Not used:  self.ui.sixBitAdc_comboBox.currentIndex()

    def set_s8b(self, value: bool):
        self.ui.eightBitConvMode_pushButton.setChecked(value)

    def set_s6b(self, value: bool):
        pass  # Not used:  int(self.ui.sixBit_pushButton.setChecked(value)

    def set_s10b(self, value: bool):
        self.ui.adcOnOff_pushButton.setChecked(value)

    def set_sdcks(self, value: bool):
        self.ui.dualClockData_pushButton.setChecked(value)

    def set_sdcka(self, value: bool):
        self.ui.dualClockArt_pushButton.setChecked(value)

    def set_sdck6b(self, value: bool):
        pass  # Not used:  int(self.ui.dualClockSixBit_pushButton.setChecked(value)

    def set_sdrv(self, value: bool):
        pass  # Not used:  int(self.ui.analogTristates_pushButton.setChecked(value)

    def set_stpp(self, value: bool):
        pass  # Not used:

    def set_sL0dckinv(self, value: bool):
        self.ui.sL0dckinv_pushButton.setChecked(value)

    def set_sL0ckinv(self, value: bool):
        self.ui.sL0ckinv_pushButton.setChecked(value)

    def set_sp(self, value):
        self.ui.inputChargePolarity_comboBox.setCurrentIndex(value)

    def set_sdp(self, value: bool):
        self.ui.disableAtPeak_pushButton.setChecked(value)

    def set_sbmx(self, value: bool):
        self.ui.sbmx_pushButton.setChecked(value)

    def set_sbft(self, value: bool):
        self.ui.sbft_pushButton.setChecked(value)

    def set_sbfp(self, value: bool):
        self.ui.sbfp_pushButton.setChecked(value)

    def set_sbfm(self, value: bool):
        self.ui.sbfm_pushButton.setChecked(value)

    def set_slg(self, value: bool):
        self.ui.leakCurr_pushButton.setChecked(value)

    def set_sm(self, value):
        self.ui.analogMonitor_comboBox.setCurrentIndex(value)

    # TODO: verify that SCMX is controlled by ReadADC pushButton
    def set_scmx(self, value: bool):
        self.ui.readADC_pushButton.setChecked(value)

    def set_sfa(self, value):
        self.ui.artMode_comboBox.setCurrentIndex(value)

    def set_sfam(self, value):
        self.ui.artMode_comboBox.setCurrentIndex(value)

    def set_st(self, value):
        self.ui.peakTime_comboBox.setCurrentIndex(value)

    def set_sfm(self, value: bool):
        self.ui.doubleLeak_pushButton.setChecked(value)

    def set_sg(self, value):
        self.ui.gain_comboBox.setCurrentIndex(value)

    def set_sng(self, value: bool):
        self.ui.neighborTrigger_pushButton.setChecked(value)

    def set_stot(self, value: bool):
        pass  # Not used:  self.ui.modeStot_comboBox.currentIndex()

    def set_sttt(self, value: bool):  # NOTE modeled as STPP, not sure if this is correct
        pass  # Not used:  self.ui.modeStpp_comboBox.currentIndex()

    def set_ssh(self, value: bool):
        self.ui.subHysterisis_pushButton.setChecked(value)

    def set_stc(self, value):
        self.ui.tacSlopAdj_comboBox.setCurrentIndex(value)

    def set_sdt_upper_4bits(self, value):
        v = value << 6
        self.ui.thresholdDac_spinBox.setValue(v & 0x3C0)

    def set_sL0cktest(self, value: bool):
        self.ui.sL0cktest_pushButton.setChecked(value)

    def set_sL0ena(self, value: bool):
        self.ui.sL0ena_pushButton.setChecked(value)

    def set_sL0enaV(self, value: bool):
        self.ui.sL0enaV_pushButton.setChecked(value)

    def set_truncate_i(self, value: bool):
        self.ui.truncate_spinBox.setValue(value)

    def set_nskip_i(self, value: bool):
        self.ui.nskip_spinBox.setValue(value)

    def set_window_i(self, value: bool):
        self.ui.window_spinBox.setValue(value)

    def set_rollover_i(self, value: bool):
        self.ui.rollover_spinBox.setValue(value)

    def set_L0offset_i(self, value: bool):
        self.ui.l0offset_spinBox.setValue(value)

    def set_offset_i(self, value: bool):
        self.ui.offset_spinBox.setValue(value)

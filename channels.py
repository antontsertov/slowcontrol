from typing import List

from PyQt5 import QtWidgets

import FEC
import Hybrid
import VMM
from channel import ChannelSettings
from data.vmmsettingsdata import Channel
from generated_ui.channels import Ui_Form as Channels


def recursive_channels(idx: int):
    if idx > 63:
        return
    channel = ChannelSettings()
    channel.set_index(idx)
    yield channel
    idx = idx + 1
    yield from recursive_channels(idx)


class ChannelsWidget(QtWidgets.QWidget):
    sc_state = False
    sl_state = False
    st_state = True
    sth_state = False
    sm_state = False
    smx_state = False
    sd_index = 0
    sz010b_index = 0
    sz08b_index = 0
    sz06b_index = 0

    def __init__(self, parent=None):
        super().__init__(parent)
        self.ui = Channels()
        self.ui.setupUi(self)
        self.ui.sc_pushButton.clicked.connect(self.sync_sc_signals)
        self.ui.sl_pushButton.clicked.connect(self.sync_sl_signals)
        self.ui.st_pushButton.clicked.connect(self.sync_st_signals)
        self.ui.sth_pushButton.clicked.connect(self.sync_sth_signals)
        self.ui.sm_pushButton.clicked.connect(self.sync_sm_signals)
        self.ui.smx_pushButton.clicked.connect(self.sync_smx_signals)
        self.ui.sd_comboBox.currentIndexChanged.connect(self.sync_sd_signals)
        self.ui.sz010b_comboBox.currentIndexChanged.connect(self.sync_sz010b_signals)
        self.ui.sz08b_comboBox.currentIndexChanged.connect(self.sync_sz08b_signals)
        self.ui.sz06b_comboBox.currentIndexChanged.connect(self.sync_sz06b_signals)

        self.channel_generator = recursive_channels(0)
        # QTimer.singleShot(0, self.create_channel)
        self.create_channel()
        self.fec_idx = 0
        self.hybrid_idx = 0
        self.vmm_idx = 0

    def set_info_label(self, fec_idx: int, hybrid_idx: int, vmm_idx: int):
        self.fec_idx = fec_idx
        self.hybrid_idx = hybrid_idx
        self.vmm_idx = vmm_idx
        title: str = "FEC: " + str(self.fec_idx + 1) + " -  Hybrid: " + str(self.hybrid_idx)\
                     + " -  VMM: " + str(self.vmm_idx + 1)
        self.ui.selection_label.setText(title)

    def update_channel_settings_in_current_vmm(self, main_window):
        fec: FEC = main_window.ui.FECs_tabWidget.widget(self.fec_idx)
        hybrid: Hybrid = fec.ui.hybrid_tabWidget.widget(self.hybrid_idx)
        vmm: VMM = hybrid.ui.VMMs_tabWidget.widget(self.vmm_idx)
        vmm.update_channel_settings(self.get_channel_settings())

    def create_channel(self):
        # Get the next channel
        try:
            channel = next(self.channel_generator)
        except StopIteration:
            return

        self.ui.scrollLayout.addWidget(channel)
        self.ui.scrollLayout.addWidget(channel)
        # Put another call to create_channel in the event Queue
        # QTimer.singleShot(0, self.create_channel)
        self.create_channel()

    def get_channel(self, idx):
        channel_widget = self.ui.scrollLayout.itemAt(idx).widget()

        channel = Channel(idx,
                          int(channel_widget.ui.sc_pushButton.isChecked()),
                          int(channel_widget.ui.sl_pushButton.isChecked()),
                          int(channel_widget.ui.st_pushButton.isChecked()),
                          int(channel_widget.ui.sth_pushButton.isChecked()),
                          int(channel_widget.ui.sm_pushButton.isChecked()),
                          channel_widget.ui.sd_comboBox.currentIndex(),
                          int(channel_widget.ui.smx_pushButton.isChecked()),
                          channel_widget.ui.sz010b_comboBox.currentIndex(),
                          channel_widget.ui.sz08b_comboBox.currentIndex(),
                          channel_widget.ui.sz06b_comboBox.currentIndex())

        return channel

    def get_channel_settings(self):
        channels: List[Channel] = []
        for idx in range(0, 64):
            channels.append(self.get_channel(idx))

        return channels

    def set_channel(self, channel: Channel):
        channel_widget = self.ui.scrollLayout.itemAt(channel.idx).widget()

        if channel_widget.ui.sc_pushButton.isChecked() != bool(channel.sc):
            channel_widget.ui.sc_pushButton.setChecked(bool(channel.sc))
        if channel_widget.ui.sl_pushButton.isChecked() != bool(channel.sl):
            channel_widget.ui.sl_pushButton.setChecked(bool(channel.sl))
        if channel_widget.ui.st_pushButton.isChecked() != bool(channel.st):
            channel_widget.ui.st_pushButton.setChecked(bool(channel.st))
        if channel_widget.ui.sth_pushButton.isChecked() != bool(channel.sth):
            channel_widget.ui.sth_pushButton.setChecked(bool(channel.sth))
        if channel_widget.ui.sm_pushButton.isChecked() != bool(channel.sm):
            channel_widget.ui.sm_pushButton.setChecked(bool(channel.sm))
        if channel_widget.ui.sd_comboBox.currentIndex() != channel.sd:
            channel_widget.ui.sd_comboBox.setCurrentIndex(channel.sd)
        if channel_widget.ui.smx_pushButton.isChecked() != bool(channel.smx):
            channel_widget.ui.smx_pushButton.setChecked(bool(channel.smx))
        if channel_widget.ui.sz010b_comboBox.currentIndex() != channel.adc0_10:
            channel_widget.ui.sz010b_comboBox.setCurrentIndex(channel.adc0_10)
        if channel_widget.ui.sz08b_comboBox.currentIndex() != channel.adc0_8:
            channel_widget.ui.sz08b_comboBox.setCurrentIndex(channel.adc0_8)
        if channel_widget.ui.sz06b_comboBox.currentIndex() != channel.adc0_6:
            channel_widget.ui.sz06b_comboBox.setCurrentIndex(channel.adc0_6)

    def sync_channel_settings(self, vmm: VMM):
        if not self.isEnabled():
            return

        vmm.update_channel_settings(self.get_channel_settings())

        main_window = self.window()
        fec_idx = main_window.ui.FECs_tabWidget.currentIndex()
        hybrid_idx = main_window.ui.FECs_tabWidget.currentWidget().ui.hybrid_tabWidget.currentIndex()
        vmm_idx = main_window.ui.FECs_tabWidget.currentWidget().ui.hybrid_tabWidget.currentWidget().ui.VMMs_tabWidget.currentIndex()
        self.set_info_label(fec_idx, hybrid_idx, vmm_idx)

    def set_vmm_channel_settings_to_ui(self, vmm: VMM):
        if not self.isEnabled():
            return

        for channel in vmm.get_channels():
            self.set_channel(channel)

        main_window = self.window()
        fec_idx = main_window.ui.FECs_tabWidget.currentIndex()
        hybrid_idx = main_window.ui.FECs_tabWidget.currentWidget().ui.hybrid_tabWidget.currentIndex()
        vmm_idx = main_window.ui.FECs_tabWidget.currentWidget().ui.hybrid_tabWidget.currentWidget().ui.VMMs_tabWidget.currentIndex()
        self.set_info_label(fec_idx, hybrid_idx, vmm_idx)

    def sync_sz06b_signals(self):
        self.sz06b_index = self.ui.sz06b_comboBox.currentIndex()
        for idx in range(0, 64):
            channel_widget = self.ui.scrollLayout.itemAt(idx).widget()
            channel_widget.set_sz06b_values()
            channel_widget.ui.sz06b_comboBox.setCurrentIndex(self.sz06b_index)

    def sync_sz08b_signals(self):
        self.sz08b_index = self.ui.sz08b_comboBox.currentIndex()

        for idx in range(0, 64):
            channel_widget = self.ui.scrollLayout.itemAt(idx).widget()
            channel_widget.set_sz08b_values()
            channel_widget.ui.sz08b_comboBox.setCurrentIndex(self.sz08b_index)

    def sync_sz010b_signals(self):
        self.sz010b_index = self.ui.sz010b_comboBox.currentIndex()

        for idx in range(0, 64):
            channel_widget = self.ui.scrollLayout.itemAt(idx).widget()
            channel_widget.set_sz010b_values()
            channel_widget.ui.sz010b_comboBox.setCurrentIndex(self.sz010b_index)

    def sync_sd_signals(self):
        self.sd_index = self.ui.sd_comboBox.currentIndex()

        for idx in range(0, 64):
            channel_widget = self.ui.scrollLayout.itemAt(idx).widget()
            channel_widget.set_sd_values()
            channel_widget.ui.sd_comboBox.setCurrentIndex(self.sd_index)

    def sync_smx_signals(self):
        self.smx_state = not self.smx_state

        for idx in range(0, 64):
            channel_widget = self.ui.scrollLayout.itemAt(idx).widget()
            channel_widget.ui.smx_pushButton.setChecked(self.smx_state)

    def sync_sm_signals(self):
        self.sm_state = not self.sm_state

        for idx in range(0, 64):
            channel_widget = self.ui.scrollLayout.itemAt(idx).widget()
            channel_widget.ui.sm_pushButton.setChecked(self.sm_state)

    def sync_sth_signals(self):
        self.sth_state = not self.sth_state

        for idx in range(0, 64):
            channel_widget = self.ui.scrollLayout.itemAt(idx).widget()
            channel_widget.ui.sth_pushButton.setChecked(self.sth_state)

    def sync_st_signals(self):
        self.st_state = not self.st_state

        for idx in range(0, 64):
            channel_widget = self.ui.scrollLayout.itemAt(idx).widget()
            channel_widget.ui.st_pushButton.setChecked(self.st_state)

    def sync_sl_signals(self):
        self.sl_state = not self.sl_state

        for idx in range(0, 64):
            channel_widget = self.ui.scrollLayout.itemAt(idx).widget()
            channel_widget.ui.sl_pushButton.setChecked(self.sl_state)

    def sync_sc_signals(self):
        self.sc_state = not self.sc_state

        for idx in range(0, 64):
            channel_widget = self.ui.scrollLayout.itemAt(idx).widget()
            channel_widget.ui.sc_pushButton.setChecked(self.sc_state)

from enum import Flag, auto


from data.vmmsettingsdata import Channel, ChannelSettings, FecList, FecData, RegisterSPI0VMMBank1, RegisterSPI0VMMBank2, \
    RegisterSPI1VMMBank1, RegisterSPI1VMMBank2, RegisterSPI2VMMBank1, RegisterSPI2VMMBank2, VmmList, VmmData, \
    HybridList, HybridData
from generated_ui.SlowControlMain_lci import Ui_MainWindow
from FEC import FEC
import main
import Hybrid
import VMM
import json
import dataclasses


class VmmConfigurationType(Flag):
    NONE = auto()
    ALL_CHANNEL = auto()
    ALL_GLOBAL = auto()
    ALL = ALL_GLOBAL | ALL_CHANNEL


def apply_vmm_settings_to_all_vmms(main_window, vmm_data, settings_type: VmmConfigurationType):
    for fec_idx in range(0, main_window.ui.FECs_tabWidget.count()):
        fec_widget = main_window.ui.FECs_tabWidget.widget(fec_idx)
        for hybrid_idx in range(0, fec_widget.ui.hybrid_tabWidget.count()):
            hybrid_widget = fec_widget.ui.hybrid_tabWidget.widget(hybrid_idx)
            for vmm_idx in range(0, hybrid_widget.ui.VMMs_tabWidget.count()):
                vmm_data.vmm_idx = vmm_idx + 1
                if settings_type & VmmConfigurationType.ALL_GLOBAL:
                    apply_vmm_global_settings(vmm_data, hybrid_widget)
                if settings_type & VmmConfigurationType.ALL_CHANNEL:
                    apply_vmm_channel_settings(vmm_data, hybrid_widget)


def set_vmm_settings_to_all_vmms(vmm: VMM, settings_type: VmmConfigurationType):
    main_window: main.SlowControlMainWindow = vmm.window()
    fec: FEC = main_window.ui.FECs_tabWidget.currentWidget()
    hybrid: Hybrid = fec.ui.hybrid_tabWidget.currentWidget()
    vmm_data = VmmData(hybrid.ui.VMMs_tabWidget.currentIndex() + 1)

    if settings_type & VmmConfigurationType.ALL_GLOBAL:
        get_vmm_global_settings(hybrid, vmm_data)
    if settings_type & VmmConfigurationType.ALL_CHANNEL:
        get_vmm_channel_settings(hybrid, vmm_data)

    apply_vmm_settings_to_all_vmms(main_window, vmm_data, settings_type)


def sync_channel_settings(vmm: VMM):
    main_window: main.SlowControlMainWindow = vmm.window()
    main_window.channels_widget.sync_channel_settings(vmm)
    main_window.channels_widget.show()


def set_vmm_channel_settings_to_ui(vmm: VMM):
    main_window: main.SlowControlMainWindow = vmm.window()
    main_window.channels_widget.set_vmm_channel_settings_to_ui(vmm)
    main_window.channels_widget.show()


def hard_reset_vmm(vmm: VMM):
    main_window: main.SlowControlMainWindow = vmm.window()
    fec: FEC = main_window.ui.FECs_tabWidget.currentWidget()
    hybrid: Hybrid = fec.ui.hybrid_tabWidget.currentWidget()
    vmm_data = VmmData(hybrid.ui.VMMs_tabWidget.currentIndex() + 1)
    apply_vmm_data(vmm_data, hybrid)


def hard_reset_all_vmm(vmm: VMM):
    main_window: main.SlowControlMainWindow = vmm.window()
    # create the default VMM settings
    vmm_data = VmmData(0)
    apply_vmm_settings_to_all_vmms(main_window, vmm_data, VmmConfigurationType.ALL)


def set_hybrid_settings_to_all_hybrids(hybrid: Hybrid):
    main_window: main.SlowControlMainWindow = hybrid.window()
    for fec_idx in range(0, main_window.ui.FECs_tabWidget.count()):
        fec_widget = main_window.ui.FECs_tabWidget.widget(fec_idx)
        for hybrid_idx in range(0, fec_widget.ui.hybrid_tabWidget.count()):
            hybrid_widget = fec_widget.ui.hybrid_tabWidget.widget(hybrid_idx)
            hybrid_widget.apply_settings_from_other_hybrid(hybrid)


def load_configuration_from_file(filename):
    with open(filename, 'r', encoding='utf-8') as f:
        configuration = json.load(f, object_hook=from_json)

    return configuration


def save_configuration_to_file(ui_main_window: Ui_MainWindow, filename):
    with open(filename, 'w', encoding='utf-8') as f:
        configuration = get_configuration(ui_main_window)
        json.dump(configuration, f, indent=2, default=to_json)


def get_configuration(ui_main_window: Ui_MainWindow):
    fec_list = FecList()

    for i in range(0, ui_main_window.numberOfCards_spinBox.value()):
        fec_data = FecData(i)
        fec = eval(f"ui_main_window.FECs_tabWidget.widget({i})")
        for hybrid_idx in range(0, 5):

            if eval(f"fec.ui.hybrid{hybrid_idx}_checkBox.isChecked()"):
                hybrid_data = HybridData(hybrid_idx)
                hybrid = fec.ui.hybrid_tabWidget.widget(hybrid_idx)
                is_vmm1_enabled = hybrid.ui.VMM1_checkBox.isChecked()
                is_vmm2_enabled = hybrid.ui.VMM2_checkBox.isChecked()

                # if at least one of VMM is enabled save the FEC tab data configuration
                if not (is_vmm1_enabled or is_vmm2_enabled):
                    continue

                for vmm_idx in range(1, 3):
                    if eval(f"is_vmm{vmm_idx}_enabled"):
                        hybrid_data.vmms.vmmList.append(get_vmm_data(hybrid, vmm_idx))
                fec_data.hybrids.append(hybrid_data)

        fec_list.append(fec_data)

    return fec_list


def get_vmm_data(hybrid: Hybrid, vmm_idx):
    vmm = VmmData(vmm_idx)
    get_vmm_channel_settings(hybrid, vmm)
    get_vmm_global_settings(hybrid, vmm)
    return vmm


def get_vmm_channel_settings(hybrid: Hybrid, vmm: VmmData):
    vmm_idx = vmm.vmm_idx
    vmm.channel_settings.channels.clear()  # previous values can be removed
    for i in range(0, 64):
        channel = hybrid.get_channel(vmm_idx, i)
        vmm.channel_settings.channels.insert(i, channel)


def get_vmm_global_settings(hybrid: Hybrid, vmm: VmmData):
    vmm_idx = vmm.vmm_idx
    set_spi0_vmm_bank1(hybrid, vmm_idx, vmm.spi0_vmm_bank1)
    set_spi0_vmm_bank2(hybrid, vmm_idx, vmm.spi0_vmm_bank2)
    set_spi1_vmm_bank1(hybrid, vmm_idx, vmm.spi1_vmm_bank1)
    set_spi1_vmm_bank2(hybrid, vmm_idx, vmm.spi1_vmm_bank2)
    set_spi2_vmm_bank1(hybrid, vmm_idx, vmm.spi2_vmm_bank1)
    set_spi2_vmm_bank2(hybrid, vmm_idx, vmm.spi2_vmm_bank2)


def set_spi0_vmm_bank1(hybrid, vmm_idx, spi0_vmm_bank1):
    spi0_vmm_bank1.sL0enaV = hybrid.get_sL0enaV(vmm_idx)
    spi0_vmm_bank1.slvs = hybrid.get_slvs(vmm_idx)
    spi0_vmm_bank1.s32 = hybrid.get_s32(vmm_idx)
    spi0_vmm_bank1.stcr = hybrid.get_stcr(vmm_idx)
    spi0_vmm_bank1.ssart = hybrid.get_ssart(vmm_idx)
    spi0_vmm_bank1.srec = hybrid.get_srec(vmm_idx)
    spi0_vmm_bank1.stlc = hybrid.get_stlc(vmm_idx)
    spi0_vmm_bank1.sbip = hybrid.get_sbip(vmm_idx)
    spi0_vmm_bank1.srat = hybrid.get_srat(vmm_idx)
    spi0_vmm_bank1.sfrst = hybrid.get_sfrst(vmm_idx)
    spi0_vmm_bank1.slvsbc = hybrid.get_slvsbc(vmm_idx)
    spi0_vmm_bank1.slvstp = hybrid.get_slvstp(vmm_idx)
    spi0_vmm_bank1.slvstk = hybrid.get_slvstk(vmm_idx)
    spi0_vmm_bank1.slvsdt = hybrid.get_slvsdt(vmm_idx)
    spi0_vmm_bank1.slvsart = hybrid.get_slvsart(vmm_idx)
    spi0_vmm_bank1.slvstki = hybrid.get_slvstki(vmm_idx)
    spi0_vmm_bank1.slvsena = hybrid.get_slvsena(vmm_idx)
    spi0_vmm_bank1.slvs6b = hybrid.get_slvs6b(vmm_idx)
    spi0_vmm_bank1.slh = hybrid.get_slh(vmm_idx)
    spi0_vmm_bank1.slxh = hybrid.get_slxh(vmm_idx)
    spi0_vmm_bank1.stgc = hybrid.get_stgc(vmm_idx)
    spi0_vmm_bank1.reset1 = hybrid.get_reset1(vmm_idx)
    spi0_vmm_bank1.reset2 = hybrid.get_reset2(vmm_idx)


def set_spi0_vmm_bank2(hybrid, vmm_idx, spi0_vmm_bank2):
    spi0_vmm_bank2.nskipm_i = hybrid.get_nskipm_i(vmm_idx)


def set_spi1_vmm_bank1(hybrid, vmm_idx, spi1_vmm_bank1):
    spi1_vmm_bank1.sdt_lower_6bits = hybrid.get_sdt_lower_6bits(vmm_idx)
    spi1_vmm_bank1.sdp10 = hybrid.get_sdp10(vmm_idx)
    spi1_vmm_bank1.sc10b = hybrid.get_sc10b(vmm_idx)
    spi1_vmm_bank1.sc8b = hybrid.get_sc8b(vmm_idx)
    spi1_vmm_bank1.sc6b = hybrid.get_sc6b(vmm_idx)
    spi1_vmm_bank1.s8b = hybrid.get_s8b(vmm_idx)
    spi1_vmm_bank1.s6b = hybrid.get_s6b(vmm_idx)
    spi1_vmm_bank1.s10b = hybrid.get_s10b(vmm_idx)
    spi1_vmm_bank1.sdcks = hybrid.get_sdcks(vmm_idx)
    spi1_vmm_bank1.sdcka = hybrid.get_sdcka(vmm_idx)
    spi1_vmm_bank1.sdck6b = hybrid.get_sdck6b(vmm_idx)
    spi1_vmm_bank1.sdrv = hybrid.get_sdrv(vmm_idx)
    spi1_vmm_bank1.stpp = hybrid.get_stpp(vmm_idx)


def set_spi1_vmm_bank2(hybrid, vmm_idx, spi1_vmm_bank2):
    spi1_vmm_bank2.sL0cktest = hybrid.get_sL0cktest(vmm_idx)
    spi1_vmm_bank2.sL0dckinv = hybrid.get_sL0dckinv(vmm_idx)
    spi1_vmm_bank2.sL0ckinv = hybrid.get_sL0ckinv(vmm_idx)
    spi1_vmm_bank2.sL0ena = hybrid.get_sL0ena(vmm_idx)
    spi1_vmm_bank2.truncate_i = hybrid.get_truncate_i(vmm_idx)
    spi1_vmm_bank2.nskip_i = hybrid.get_nskip_i(vmm_idx)
    spi1_vmm_bank2.window_i = hybrid.get_window_i(vmm_idx)
    spi1_vmm_bank2.rollover_i = hybrid.get_rollover_i(vmm_idx)


def set_spi2_vmm_bank1(hybrid, vmm_idx, spi2_vmm_bank1):
    spi2_vmm_bank1.sp = hybrid.get_sp(vmm_idx)
    spi2_vmm_bank1.sdp = hybrid.get_sdp(vmm_idx)
    spi2_vmm_bank1.sbmx = hybrid.get_sbmx(vmm_idx)
    spi2_vmm_bank1.sbft = hybrid.get_sbft(vmm_idx)
    spi2_vmm_bank1.sbfp = hybrid.get_sbfp(vmm_idx)
    spi2_vmm_bank1.sbfm = hybrid.get_sbfm(vmm_idx)
    spi2_vmm_bank1.slg = hybrid.get_slg(vmm_idx)
    spi2_vmm_bank1.sm = hybrid.get_sm(vmm_idx)  # monitoring
    spi2_vmm_bank1.scmx = hybrid.get_scmx(vmm_idx)
    spi2_vmm_bank1.sfa = hybrid.get_sfa(vmm_idx)
    spi2_vmm_bank1.sfam = hybrid.get_sfam(vmm_idx)
    spi2_vmm_bank1.st = hybrid.get_st(vmm_idx)  # peaktime
    spi2_vmm_bank1.sfm = hybrid.get_sfm(vmm_idx)
    spi2_vmm_bank1.sg = hybrid.get_sg(vmm_idx)  # gain
    spi2_vmm_bank1.sng = hybrid.get_sng(vmm_idx)
    spi2_vmm_bank1.stot = hybrid.get_stot(vmm_idx)
    spi2_vmm_bank1.sttt = hybrid.get_sttt(vmm_idx)
    spi2_vmm_bank1.ssh = hybrid.get_ssh(vmm_idx)
    spi2_vmm_bank1.stc = hybrid.get_stc(vmm_idx)
    spi2_vmm_bank1.sdt_upper_4bits = hybrid.get_sdt_upper_4bits(vmm_idx)


def set_spi2_vmm_bank2(hybrid, vmm_idx, spi2_vmm_bank2):
    spi2_vmm_bank2.L0offset_i = hybrid.get_L0offset_i(vmm_idx)
    spi2_vmm_bank2.offset_i = hybrid.get_offset_i(vmm_idx)


def apply_fec_configuration(ui_main_window: Ui_MainWindow, fec_data: FecData):
    fec_widget = ui_main_window.FECs_tabWidget.widget(fec_data.fec_idx)
    fec_widget.ui.hybrid0_checkBox.setChecked(False)
    fec_widget.ui.hybrid1_checkBox.setChecked(False)
    fec_widget.ui.hybrid2_checkBox.setChecked(False)
    fec_widget.ui.hybrid3_checkBox.setChecked(False)
    fec_widget.ui.hybrid4_checkBox.setChecked(False)
    ui_main_window.numberOfCards_spinBox.setValue(fec_data.fec_idx + 1)


def apply_hybrid_configuration(ui_main_window: Ui_MainWindow, hybrid_data, fec_idx):
    fec_widget = ui_main_window.FECs_tabWidget.widget(fec_idx)
    eval(f"fec_widget.ui.hybrid{hybrid_data.hybrid_idx}_checkBox.setChecked(True)")
    hybrid_widget = fec_widget.ui.hybrid_tabWidget.widget(hybrid_data.hybrid_idx)
    hybrid_widget.ui.VMM1_checkBox.setChecked(False)
    hybrid_widget.ui.VMM2_checkBox.setChecked(False)


def apply_vmm_configuration(ui_main_window: Ui_MainWindow, vmm_data, fec_idx, hybrid_idx):
    fec_widget = ui_main_window.FECs_tabWidget.widget(fec_idx)
    hybrid_widget = fec_widget.ui.hybrid_tabWidget.widget(hybrid_idx)
    if not eval(f"hybrid_widget.ui.VMM{vmm_data.vmm_idx}_checkBox.isChecked()"):
        eval(f"hybrid_widget.ui.VMM{vmm_data.vmm_idx}_checkBox.setChecked(True)")
    apply_vmm_data(vmm_data, hybrid_widget)


def apply_spi0_vmm_bank1(vmm_data: VmmData, hybrid_widget: Hybrid):
    spi0_vmm_bank1: RegisterSPI0VMMBank1 = vmm_data.spi0_vmm_bank1
    vmm_idx = vmm_data.vmm_idx

    hybrid_widget.set_sL0enaV(vmm_idx, spi0_vmm_bank1.sL0enaV)
    hybrid_widget.set_slvs(vmm_idx, spi0_vmm_bank1.slvs)
    hybrid_widget.set_s32(vmm_idx, spi0_vmm_bank1.s32)
    hybrid_widget.set_stcr(vmm_idx, spi0_vmm_bank1.stcr)
    hybrid_widget.set_ssart(vmm_idx, spi0_vmm_bank1.ssart)
    hybrid_widget.set_srec(vmm_idx, spi0_vmm_bank1.srec)
    hybrid_widget.set_stlc(vmm_idx, spi0_vmm_bank1.stlc)
    hybrid_widget.set_sbip(vmm_idx, spi0_vmm_bank1.sbip)
    hybrid_widget.set_srat(vmm_idx, spi0_vmm_bank1.srat)
    hybrid_widget.set_sfrst(vmm_idx, spi0_vmm_bank1.sfrst)
    hybrid_widget.set_slvsbc(vmm_idx, spi0_vmm_bank1.slvsbc)
    hybrid_widget.set_slvstp(vmm_idx, spi0_vmm_bank1.slvstp)
    hybrid_widget.set_slvstk(vmm_idx, spi0_vmm_bank1.slvstk)
    hybrid_widget.set_slvsdt(vmm_idx, spi0_vmm_bank1.slvsdt)
    hybrid_widget.set_slvsart(vmm_idx, spi0_vmm_bank1.slvsart)
    hybrid_widget.set_slvstki(vmm_idx, spi0_vmm_bank1.slvstki)
    hybrid_widget.set_slvsena(vmm_idx, spi0_vmm_bank1.slvsena)
    hybrid_widget.set_slvs6b(vmm_idx, spi0_vmm_bank1.slvs6b)
    hybrid_widget.set_slh(vmm_idx, spi0_vmm_bank1.slh)
    hybrid_widget.set_slxh(vmm_idx, spi0_vmm_bank1.slxh)
    hybrid_widget.set_stgc(vmm_idx, spi0_vmm_bank1.stgc)
    hybrid_widget.set_reset1(vmm_idx, spi0_vmm_bank1.reset1)
    hybrid_widget.set_reset2(vmm_idx, spi0_vmm_bank1.reset2)


def apply_spi0_vmm_bank2(vmm_data: VmmData, hybrid_widget: Hybrid):
    hybrid_widget.set_nskipm_i(vmm_data.vmm_idx, vmm_data.spi0_vmm_bank2.nskipm_i)


def apply_spi1_vmm_bank1(vmm_data: VmmData, hybrid_widget: Hybrid):
    spi1_vmm_bank1: RegisterSPI1VMMBank1 = vmm_data.spi1_vmm_bank1
    vmm_idx = vmm_data.vmm_idx

    hybrid_widget.set_sdt_lower_6bits(vmm_idx, spi1_vmm_bank1.sdt_lower_6bits)
    hybrid_widget.set_sdp10(vmm_idx, spi1_vmm_bank1.sdp10)
    hybrid_widget.set_sc10b(vmm_idx, spi1_vmm_bank1.sc10b)
    hybrid_widget.set_sc8b(vmm_idx, spi1_vmm_bank1.sc8b)
    hybrid_widget.set_sc6b(vmm_idx, spi1_vmm_bank1.sc6b)
    hybrid_widget.set_s8b(vmm_idx, spi1_vmm_bank1.s8b)
    hybrid_widget.set_s6b(vmm_idx, spi1_vmm_bank1.s6b)
    hybrid_widget.set_s10b(vmm_idx, spi1_vmm_bank1.s10b)
    hybrid_widget.set_sdcks(vmm_idx, spi1_vmm_bank1.sdcks)
    hybrid_widget.set_sdcka(vmm_idx, spi1_vmm_bank1.sdcka)
    hybrid_widget.set_sdck6b(vmm_idx, spi1_vmm_bank1.sdck6b)
    hybrid_widget.set_sdrv(vmm_idx, spi1_vmm_bank1.sdrv)
    hybrid_widget.set_stpp(vmm_idx, spi1_vmm_bank1.stpp)


def apply_spi1_vmm_bank2(vmm_data: VmmData, hybrid_widget: Hybrid):
    spi1_vmm_bank2: RegisterSPI1VMMBank2 = vmm_data.spi1_vmm_bank2
    vmm_idx = vmm_data.vmm_idx

    hybrid_widget.set_sL0cktest(vmm_idx, spi1_vmm_bank2.sL0cktest)
    hybrid_widget.set_sL0dckinv(vmm_idx, spi1_vmm_bank2.sL0dckinv)
    hybrid_widget.set_sL0ckinv(vmm_idx, spi1_vmm_bank2.sL0ckinv)
    hybrid_widget.set_sL0ena(vmm_idx, spi1_vmm_bank2.sL0ena)
    hybrid_widget.set_truncate_i(vmm_idx, spi1_vmm_bank2.truncate_i)
    hybrid_widget.set_nskip_i(vmm_idx, spi1_vmm_bank2.nskip_i)
    hybrid_widget.set_window_i(vmm_idx, spi1_vmm_bank2.window_i)
    hybrid_widget.set_rollover_i(vmm_idx, spi1_vmm_bank2.rollover_i)


def apply_spi2_vmm_bank1(vmm_data: VmmData, hybrid_widget: Hybrid):
    spi2_vmm_bank1: RegisterSPI2VMMBank1 = vmm_data.spi2_vmm_bank1
    vmm_idx = vmm_data.vmm_idx

    hybrid_widget.set_sp(vmm_idx, spi2_vmm_bank1.sp)
    hybrid_widget.set_sdp(vmm_idx, spi2_vmm_bank1.sdp)
    hybrid_widget.set_sbmx(vmm_idx, spi2_vmm_bank1.sbmx)
    hybrid_widget.set_sbft(vmm_idx, spi2_vmm_bank1.sbft)
    hybrid_widget.set_sbfp(vmm_idx, spi2_vmm_bank1.sbfp)
    hybrid_widget.set_sbfm(vmm_idx, spi2_vmm_bank1.sbfm)
    hybrid_widget.set_slg(vmm_idx, spi2_vmm_bank1.slg)
    hybrid_widget.set_sm(vmm_idx, spi2_vmm_bank1.sm)  # monitoring
    hybrid_widget.set_scmx(vmm_idx, spi2_vmm_bank1.scmx)
    hybrid_widget.set_sfa(vmm_idx, spi2_vmm_bank1.sfa)
    hybrid_widget.set_sfam(vmm_idx, spi2_vmm_bank1.sfam)
    hybrid_widget.set_st(vmm_idx, spi2_vmm_bank1.st)  # peaktime
    hybrid_widget.set_sfm(vmm_idx, spi2_vmm_bank1.sfm)
    hybrid_widget.set_sg(vmm_idx, spi2_vmm_bank1.sg)  # gain
    hybrid_widget.set_sng(vmm_idx, spi2_vmm_bank1.sng)
    hybrid_widget.set_stot(vmm_idx, spi2_vmm_bank1.stot)
    hybrid_widget.set_sttt(vmm_idx, spi2_vmm_bank1.sttt)
    hybrid_widget.set_ssh(vmm_idx, spi2_vmm_bank1.ssh)
    hybrid_widget.set_stc(vmm_idx, spi2_vmm_bank1.stc)
    hybrid_widget.set_sdt_upper_4bits(vmm_idx, spi2_vmm_bank1.sdt_upper_4bits)


def apply_spi2_vmm_bank2(vmm_data: VmmData, hybrid_widget: Hybrid):
    spi2_vmm_bank2: RegisterSPI2VMMBank2 = vmm_data.spi2_vmm_bank2
    vmm_idx = vmm_data.vmm_idx

    hybrid_widget.set_L0offset_i(vmm_idx, spi2_vmm_bank2.L0offset_i)
    hybrid_widget.set_offset_i(vmm_idx, spi2_vmm_bank2.offset_i)


def apply_vmm_data(vmm_data: VmmData, hybrid_widget: Hybrid):
    apply_vmm_channel_settings(vmm_data, hybrid_widget)
    apply_vmm_global_settings(vmm_data, hybrid_widget)


def apply_vmm_global_settings(vmm_data: VmmData, hybrid_widget: Hybrid):
    apply_spi0_vmm_bank1(vmm_data, hybrid_widget)
    apply_spi0_vmm_bank2(vmm_data, hybrid_widget)
    apply_spi1_vmm_bank1(vmm_data, hybrid_widget)
    apply_spi1_vmm_bank2(vmm_data, hybrid_widget)
    apply_spi2_vmm_bank1(vmm_data, hybrid_widget)
    apply_spi2_vmm_bank2(vmm_data, hybrid_widget)


def apply_vmm_channel_settings(vmm_data: VmmData, hybrid_widget: Hybrid):
    channel_settings = vmm_data.channel_settings
    ch_iter = iter(channel_settings.channels)
    apply_next_vmm_channel(hybrid_widget, ch_iter, vmm_data.vmm_idx)


def apply_next_vmm_channel(hybrid_widget, ch_iter, vmm_idx):
    try:
        channel = next(ch_iter)
    except StopIteration:
        return

    hybrid_widget.update_channel(channel, vmm_idx)
    apply_next_vmm_channel(hybrid_widget, ch_iter, vmm_idx)


def to_json(python_object):
    if isinstance(python_object, FecList):
        return {'__class__': 'FecList',
                '__value__': python_object.get_as_list()}
    if isinstance(python_object, FecData):
        return {'__class__': 'FecData',
                '__value__': {
                    "fec_idx": python_object.fec_idx,
                    "hybrids": to_json(python_object.hybrids)}}
    if isinstance(python_object, HybridList):
        return {'__class__': 'HybridList',
                '__value__': python_object.get_as_list()}
    if isinstance(python_object, HybridData):
        return {'__class__': 'HybridData',
                '__value__': {
                    "hybrid_idx": python_object.hybrid_idx,
                    "vmms": to_json(python_object.vmms)}}
    if isinstance(python_object, VmmData):
        return {'__class__': 'VmmData',
                '__value__': {
                    "vmm_idx": python_object.vmm_idx,
                    "spi0_vmm_bank1": to_json(python_object.spi0_vmm_bank1),
                    "spi0_vmm_bank2": to_json(python_object.spi0_vmm_bank2),
                    "spi1_vmm_bank1": to_json(python_object.spi1_vmm_bank1),
                    "spi1_vmm_bank2": to_json(python_object.spi1_vmm_bank2),
                    "spi2_vmm_bank1": to_json(python_object.spi2_vmm_bank1),
                    "spi2_vmm_bank2": to_json(python_object.spi2_vmm_bank2),
                    "channel_settings": to_json(python_object.channel_settings)}}
    if isinstance(python_object, RegisterSPI0VMMBank1):
        return {'__class__': 'RegisterSPI0VMMBank1',
                '__value__': dataclasses.asdict(python_object)}
    if isinstance(python_object, RegisterSPI0VMMBank2):
        return {'__class__': 'RegisterSPI0VMMBank2',
                '__value__': dataclasses.asdict(python_object)}
    if isinstance(python_object, RegisterSPI1VMMBank1):
        return {'__class__': 'RegisterSPI1VMMBank1',
                '__value__': dataclasses.asdict(python_object)}
    if isinstance(python_object, RegisterSPI1VMMBank2):
        return {'__class__': 'RegisterSPI1VMMBank2',
                '__value__': dataclasses.asdict(python_object)}
    if isinstance(python_object, RegisterSPI2VMMBank1):
        return {'__class__': 'RegisterSPI2VMMBank1',
                '__value__': dataclasses.asdict(python_object)}
    if isinstance(python_object, RegisterSPI2VMMBank2):
        return {'__class__': 'RegisterSPI2VMMBank2',
                '__value__': dataclasses.asdict(python_object)}
    if isinstance(python_object, VmmList):
        return {'__class__': 'VmmList',
                '__value__': python_object.get_as_list()}
    if isinstance(python_object, ChannelSettings):
        return {'__class__': 'ChannelSettings',
                '__value__': python_object.get_as_list()}
    if isinstance(python_object, Channel):
        return {'__class__': 'Channel',
                '__value__': dataclasses.asdict(python_object)}
    raise TypeError(repr(python_object) + ' is not JSON serializable')


def from_json(json_object):
    if '__class__' in json_object:
        if json_object['__class__'] == 'FecList':
            return FecList(json_object['__value__'])
        if json_object['__class__'] == 'FecData':
            return FecData(**json_object['__value__'])
        if json_object['__class__'] == 'RegisterSPI0VMMBank1':
            return RegisterSPI0VMMBank1(**json_object['__value__'])
        if json_object['__class__'] == 'RegisterSPI0VMMBank2':
            return RegisterSPI0VMMBank2(**json_object['__value__'])
        if json_object['__class__'] == 'RegisterSPI1VMMBank1':
            return RegisterSPI1VMMBank1(**json_object['__value__'])
        if json_object['__class__'] == 'RegisterSPI1VMMBank2':
            return RegisterSPI1VMMBank2(**json_object['__value__'])
        if json_object['__class__'] == 'RegisterSPI2VMMBank1':
            return RegisterSPI2VMMBank1(**json_object['__value__'])
        if json_object['__class__'] == 'RegisterSPI2VMMBank2':
            return RegisterSPI2VMMBank2(**json_object['__value__'])
        if json_object['__class__'] == 'HybridList':
            return HybridList(json_object['__value__'])
        if json_object['__class__'] == 'HybridData':
            return HybridData(**json_object['__value__'])
        if json_object['__class__'] == 'VmmList':
            return VmmList(json_object['__value__'])
        if json_object['__class__'] == 'VmmData':
            return VmmData(**json_object['__value__'])
        if json_object['__class__'] == 'ChannelSettings':
            return ChannelSettings(json_object['__value__'])
        if json_object['__class__'] == 'Channel':
            return Channel(**json_object['__value__'])
    return json_object


def config_to_registers(configuration: FecList):
    iterator = configuration.next_vmm(0, 0, 0)
    get_vmm_registers(iterator)

    return None


def get_vmm_registers(iterator):
    try:
        vmm_data, fec_idx, hybrid_idx = next(iterator)
    except StopIteration:
        return

    for channel in vmm_data.channel_settings.channels:
        print("fec_idx: " + str(fec_idx) +
              " hybrid_idx: " + str(hybrid_idx) +
              " vmm_idx: " + str(vmm_data.vmm_idx) +
              " channel idx: " + str(channel.idx) + " reg: " + hex(channel))

    get_vmm_registers(iterator)


def create_w_access_file(message: str, configuration: FecList,
                         fec_idx: int = 0, hybrid_idx: int = 0, vmm_idx: int = 0):
    iterator = configuration.next_vmm(fec_idx, hybrid_idx, vmm_idx)
    try:
        vmm_data, fec_idx, hybrid_idx = next(iterator)
    except StopIteration:
        return

    f = open("temp_access.txt", "w+")
    line_idx: int = 0
    f.write(str(line_idx) + " MSG " + message + "\n")
    line_idx += 1

    f.write(str(line_idx) + " SET M RegisterSPI0VMMBank2 " + hex(vmm_data.spi0_vmm_bank2) + "\n")
    line_idx += 1

    f.write(str(line_idx) + " SET M RegisterSPI1VMMBank2 " + hex(vmm_data.spi1_vmm_bank2) + "\n")
    line_idx += 1

    f.write(str(line_idx) + " SET M RegisterSPI2VMMBank2 " + hex(vmm_data.spi2_vmm_bank2) + "\n")
    line_idx += 1

    f.write(str(line_idx) + " SET M RegisterSPI0VMMBank1 " + hex(vmm_data.spi0_vmm_bank1) + "\n")
    line_idx += 1

    f.write(str(line_idx) + " SET M RegisterSPI1VMMBank1 " + hex(vmm_data.spi1_vmm_bank1) + "\n")
    line_idx += 1

    f.write(str(line_idx) + " SET M RegisterSPI2VMMBank1 " + hex(vmm_data.spi2_vmm_bank1) + "\n")
    line_idx += 1

    for channel in vmm_data.channel_settings.channels:
        f.write(str(line_idx) + " SET M VMMChannel" + str(channel.idx) + " " + hex(channel) + "\n")
        line_idx += 1

    f.close()

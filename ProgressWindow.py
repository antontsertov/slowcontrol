from PyQt5.QtCore import QThread, pyqtSlot, pyqtSignal, Qt
from PyQt5 import QtWidgets


from data.vmmsettingsdata import FecList, FecData, HybridData, VmmData
from generated_ui.progress_frame import Ui_Form as ProgressFrame


class MyWorker(QThread):
    finished = pyqtSignal()
    fecDataNext = pyqtSignal(FecData)
    hybridDataNext = pyqtSignal(HybridData, int)
    vmmDataNext = pyqtSignal(VmmData, int, int)
    fecDataFin = pyqtSignal()
    intReady = pyqtSignal(int)
    beginConfigSig = pyqtSignal(FecList)

    @pyqtSlot(str)
    def send_config(self, main):  # A slot takes no params
        main.update_channel_settings()
        import configurationhandler
        configuration = configurationhandler.get_configuration(main.ui)
        # registers = configurationhandler.config_to_registers(configuration)
        fec_idx, hybrid_idx, vmm_idx = main.get_active_idx()
        configurationhandler.create_w_access_file("WRITING Configuration to FEC_" + str(fec_idx + 1)
                                                  + " Hybrid_" + str(hybrid_idx)
                                                  + " VMM_" + str(vmm_idx + 1),
                                                  configuration, fec_idx, hybrid_idx, vmm_idx)

    @pyqtSlot(str)
    def read_config(self, filename: str):  # A slot takes no params
        import configurationhandler
        configuration = configurationhandler.load_configuration_from_file(filename)
        self.beginConfigSig.emit(configuration)

    @pyqtSlot(FecList)
    def proc_config(self, configuration: FecList):  # A slot takes no params
        fec_idx = 0
        for fec_data in configuration.fecList:

            self.fecDataNext.emit(fec_data)
            self.intReady.emit(fec_idx)
            # Iterate over every Hybrid data from JSON file
            hybrid_idx = 0

            for hybrid_data in fec_data.hybrids.hybridList:

                self.hybridDataNext.emit(hybrid_data, fec_idx)

                for vmm_data in hybrid_data.vmms.vmmList:

                    self.vmmDataNext.emit(vmm_data, fec_idx, hybrid_idx)

                hybrid_idx = hybrid_idx+1
            fec_idx = fec_idx+1

        self.finished.emit()


class ProgressWindow(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super().__init__()
        self.ui = ProgressFrame()
        self.ui.setupUi(self)
        self.ui.progressBar.setTextVisible(False)
        self.setWindowFlags(Qt.FramelessWindowHint)

    def start_progress(self):  # To restart the progress every time
        self.ui.progressBar.setValue(1)
        self.show()

    def fin_progress(self):
        self.ui.progressBar.setValue(99)
        self.ui.label.setText("Almost Done, updating UI ...")
        self.hide()

    def on_count_changed(self, value):
        self.ui.progressBar.setValue(value * 10)

    def log_changed(self, log):
        self.ui.label.setText(log)
